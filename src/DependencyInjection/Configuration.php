<?php

	namespace Sixnapps\CanvasTemplateBundle\DependencyInjection;

	use Symfony\Component\Config\Definition\Builder\TreeBuilder;
	use Symfony\Component\Config\Definition\ConfigurationInterface;

	class Configuration implements ConfigurationInterface
	{
		/**
		 * @return TreeBuilder
		 */
		public function getConfigTreeBuilder()
		{
			$treeBuilder = new TreeBuilder( 'sixnapps_canvas_template' );

			if ( method_exists( $treeBuilder, 'getRootNode' ) ) {
				$rootNode = $treeBuilder->getRootNode();
			} else {
				// BC layer for symfony/config 4.1 and older
				$rootNode = $treeBuilder->root( "sixnapps_canvas_template" );
			}

			$rootNode
				->children()
					->arrayNode( 'template' )
						->children()
							->arrayNode( 'footer' )
								->children()
									->scalarNode( 'type' )->end()
								->end()
							->end()
						->end()
                        ->children()
                            ->arrayNode('colors')
                                ->children()
                                    ->scalarNode('light_primary')->defaultValue('#a8b6bf')->end()
                                    ->scalarNode('dark_primary')->defaultValue('#a8b6bf')->end()
                                    ->scalarNode('dark_title')->defaultValue('#edd9c0')->end()
                                    ->scalarNode('dark_text')->defaultValue('#ffffff')->end()
                                    ->scalarNode('dark_input')->defaultValue('#ffffff')->end()
                                ->end()
                            ->end()
                        ->end()
					->end()
				->end()
                ->children()
                    ->scalarNode('back_office')->defaultValue(false)->end()
                ->end()
			;

			return $treeBuilder;
		}
	}
