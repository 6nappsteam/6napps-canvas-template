<?php

namespace Sixnapps\CanvasTemplateBundle\Controllers;

use App\Entity\CanvasCouleur;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class CustomStylesController
 *
 * @package Sixnapps\CanvasTemplateBundle\Controllers
 */
class CustomStylesController extends AbstractController
{
    private $em;

    public function __construct( EntityManagerInterface $entityManager )
    {
        $this->em = $entityManager;
    }


    /**
     * @return Response
     */
    public function styles()
    {
        //Data du yaml
        $data = $this->container->getParameter( 'sixnapps_canvas_template' );

        //if back_office is defined and true get data bdd
        if ( array_key_exists( 'back_office', $data ) && $data[ 'back_office' ] == TRUE ) {
            //Get colors form bdd
            $couleurs = $this->em->getRepository( CanvasCouleur::class )->findAll();
            $colors = $this->setColors();
            /**
             * @var $couleur CanvasCouleur
             */
            foreach ( $couleurs as $couleur ) {
                if ( array_key_exists( $couleur->getSlug(), $colors ) ) {
                    $colors[ $couleur->getSlug() ] = $couleur->getValue();
                }

            }
        }
        else {
            $colors = $this->setColors();

        }

        $css = $this->renderView( '@SixnappsCanvasTemplate/custom-styles/colors.css.twig', [
            'colors' => $colors,
        ] );
        $response = new Response(
            $this->minimizeCSSsimple( $css ),
            Response::HTTP_OK,
            [
                'content-type'  => 'text/css',
                'Cache-Control' => 'public',
            ]
        );
        return $response;
    }


    /**
     * @return |null
     */
    private function setColors()
    {
        $colors = NULL;
        $data = $this->container->getParameter( 'sixnapps_canvas_template' );
        if ( array_key_exists( 'template', $data ) ) {
            if ( array_key_exists( 'colors', $data[ 'template' ] ) ) {
                $colors = $data[ 'template' ][ 'colors' ];
            }
        }
        return $colors;
    }


    /**
     * @param $css
     *
     * @return mixed|string|string[]|null
     */
    private function minimizeCSSsimple( $css )
    {
        $css = preg_replace( '/\/\*((?!\*\/).)*\*\//', '', $css ); // negative look ahead
        $css = preg_replace( '/\s{2,}/', ' ', $css );
        $css = preg_replace( '/\s*([:;{}])\s*/', '$1', $css );
        $css = preg_replace( '/;}/', '}', $css );
        $css = preg_replace( '!/\*[^*]*\*+([^/][^*]*\*+)*/!', '', $css );
        // Remove space after colons
        $css = str_replace( ': ', ':', $css );
        // Remove whitespace
        $css = str_replace( [ "\r\n", "\r", "\n", "\t", '  ', '    ', '    ' ], '', $css );
        return $css;
    }


    /**
     * @return Response
     */
    public function stylesDark()
    {
        $data = $this->container->getParameter( 'sixnapps_canvas_template' );
        if ( array_key_exists( 'back_office', $data ) && $data[ 'back_office' ] == TRUE ) {
            $couleurs = $this->em->getRepository( CanvasCouleur::class )->findAll();
            $colors = $this->setColors();
            /**
             * @var $couleur CanvasCouleur
             */
            foreach ( $couleurs as $couleur ) {
                if ( array_key_exists( $couleur->getSlug(), $colors ) ) {
                    $colors[ $couleur->getSlug() ] = $couleur->getValue();
                }
                if ( array_key_exists( $couleur->getSlug(), $colors ) ) {
                    $colors[ $couleur->getSlug() ] = $couleur->getValue();
                }

            }
        }
        else {
            $colors = $this->setColors();
        }

        $css = $this->renderView( '@SixnappsCanvasTemplate/custom-styles/custom-dark.css.twig', [
            'colors' => $colors,
        ] );
        $response = new Response(
            $this->minimizeCSSsimple( $css ),
            Response::HTTP_OK,
            [
                'content-type'  => 'text/css',
                'Cache-Control' => 'public',
            ]
        );
        return $response;
    }


    /**
     * @return Response
     */
    public function stylesAll()
    {
        $data = $this->container->getParameter( 'sixnapps_canvas_template' );
        if ( array_key_exists( 'back_office', $data ) && $data[ 'back_office' ] == TRUE ) {
            $couleurs = $this->em->getRepository( CanvasCouleur::class )->findAll();
            $colors = $this->setColors();
            /**
             * @var $couleur CanvasCouleur
             */
            foreach ( $couleurs as $couleur ) {
                if ( array_key_exists( $couleur->getSlug(), $colors ) ) {
                    $colors[ $couleur->getSlug() ] = $couleur->getValue();
                }
                if ( array_key_exists( $couleur->getSlug(), $colors ) ) {
                    $colors[ $couleur->getSlug() ] = $couleur->getValue();
                }

            }
        }
        else {
            $colors = $this->setColors();
        }
        $css = $this->renderView( '@SixnappsCanvasTemplate/custom-styles/custom-style.css.twig', [
            'colors' => $colors,
        ] );
        $response = new Response(
            $this->minimizeCSSsimple( $css ),
            Response::HTTP_OK,
            [
                'content-type'  => 'text/css',
                'Cache-Control' => 'public',
            ]
        );
        return $response;
    }
}
