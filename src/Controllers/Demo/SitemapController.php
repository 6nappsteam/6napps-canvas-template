<?php
	
	namespace Sixnapps\CanvasTemplateBundle\Controllers\Demo;
	
	use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
	
	/**
	 * Class SitemapController
	 *
	 * @package Sixnapps\CanvasTemplateBundle\Controllers
	 */
	class SitemapController extends AbstractController
	{
		/**
		 * @return \Symfony\Component\HttpFoundation\Response
		 */
		public function sitemap()
		{
			return $this->render( '@SixnappsCanvasTemplate/sitemap.html.twig' );
		}
	}
