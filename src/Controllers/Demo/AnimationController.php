<?php
	
	namespace Sixnapps\CanvasTemplateBundle\Controllers\Demo;
	
	use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
	
	/**
	 * Class AnimationController
	 *
	 * @package Sixnapps\CanvasTemplateBundle\Controllers
	 */
	class AnimationController extends AbstractController
	{
		/**
		 * @return \Symfony\Component\HttpFoundation\Response
		 */
		public function animation()
		{
			
			// TODO : Il y a une plusieurs version d'une même animation ( gauche, droite, haut, bas ) que je n'ai pas mmis pour pas surcharger la page.
			
			$animations = [
				[
					'word'    => 'Jello',
					'animate' => 'jello',
				],
				[
					'word'    => 'RubberBand',
					'animate' => 'rubberBand',
				],
				[
					'word'    => 'Bounce',
					'animate' => 'bounce',
				],
				[
					'word'    => 'Flash',
					'animate' => 'flash',
				],
				[
					'word'    => 'Pulse',
					'animate' => 'pulse',
				],
				[
					'word'    => 'Shake',
					'animate' => 'shake',
				],
				[
					'word'    => 'Swing',
					'animate' => 'swing',
				],
				[
					'word'    => 'Tada',
					'animate' => 'tada',
				],
				[
					'word'    => 'Wobble',
					'animate' => 'wobble',
				],
				[
					'word'    => 'BounceIn',
					'animate' => 'bounceIn',
				],
				[
					'word'    => 'FadeIn',
					'animate' => 'fadeIn',
				],
				[
					'word'    => 'Flip',
					'animate' => 'flip',
				],
				[
					'word'    => 'LightSpeedIn',
					'animate' => 'lightSpeedIn',
				],
				[
					'word'    => 'RotateIn',
					'animate' => 'rotateIn',
				],
				[
					'word'    => 'RollIn',
					'animate' => 'rollIn',
				],
				[
					'word'    => 'JackInTheBox',
					'animate' => 'jackInTheBox',
				],
				[
					'word'    => 'FadeIn',
					'animate' => 'fadeIn',
					'delay'   => '3000',
				],
			];
			
			return $this->render( '@SixnappsCanvasTemplate/animation.html.twig', [
				'animations' => $animations,
			] );
		}
	}
