<?php
	
	namespace Sixnapps\CanvasTemplateBundle\Controllers\Demo;
	
	use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
	
	/**
	 * Class GalleryController
	 *
	 * @package Sixnapps\CanvasTemplateBundle\Controllers
	 */
	class GalleryController extends AbstractController
	{
		/**
		 * @return \Symfony\Component\HttpFoundation\Response
		 */
		public function gallery()
		{
			$galleries = [
				[
					'columns'  => 2,
					'pictures' => [
						'1.jpg',
						'2.jpg',
						'5.jpg',
						'3.jpg',
						'4.jpg',
					],
					'bigData'  => 3,
				],
				[
					'columns'  => 3,
					'pictures' => [
						'5.jpg',
						'2.jpg',
						'6.jpg',
						'9.jpg',
						'8.jpg',
						'4.jpg',
					
					],
				],
				[
					'columns'  => 4,
					'pictures' => [
						'2.jpg',
						'3.jpg',
						'5.jpg',
						'8.jpg',
						'1.jpg',
						'4.jpg',
						'9.jpg',
						'7.jpg',
						'6.jpg',
					],
					'bigData'  => 2,
				],
			];
			
			return $this->render( '@SixnappsCanvasTemplate/gallery.html.twig', [
				'galleries' => $galleries,
			] );
		}
	}
