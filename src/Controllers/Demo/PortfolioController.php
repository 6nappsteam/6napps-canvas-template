<?php
	
	namespace Sixnapps\CanvasTemplateBundle\Controllers\Demo;
	
	use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
	
	/**
	 * Class PortfolioController
	 *
	 * @package Sixnapps\CanvasTemplateBundle\Controllers
	 */
	class PortfolioController extends AbstractController
	{
		/**
		 * @return \Symfony\Component\HttpFoundation\Response
		 */
		public function portfolio1()
		{
			return $this->render( '@SixnappsCanvasTemplate/portfolio.html.twig' );
		}
		
		
		/**
		 * @return \Symfony\Component\HttpFoundation\Response
		 */
		public function indexPortfolio3()
		{
			return $this->render( '@SixnappsCanvasTemplate/index-portfolio-3.html.twig' );
		}
	}
