<?php
	
	namespace Sixnapps\CanvasTemplateBundle\Controllers\Demo;
	
	use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
	
	/**
	 * Class FlipCardsController
	 *
	 * @package Sixnapps\CanvasTemplateBundle\Controllers
	 */
	class FlipCardsController extends AbstractController
	{
		/**
		 * @return \Symfony\Component\HttpFoundation\Response
		 */
		public function flipCards()
		{
			$flipCards = [
				[
					'type'     => 'petite',
					'vertical' => TRUE,
					'title'    => 'Ceci est un titre',
					'content'  => 'Je ne suis pas contre la justice tant qu\'est juste',
					'button'   => 'Venez voir ça !',
				],
				[
					'type'    => 'moyenne',
					'title'   => 'Un petit poisson',
					'content' => [
						'front' => 'Nul(le) ne peut être vainqueur contre moi',
						'back'  => 'Ou peut être que si finalement...',
					],
					'button'  => 'Cliquez pour en savoir plus',
				
				],
				[
					'type'    => 'grande',
					'title'   => 'Je suis une grande carte !',
					'content' => [
						'front' => 'Je suis le texte sur le devant de la carte',
						'back'  => 'Et moi je suis le texte sur le verso de la carte !',
					],
					'button'  => 'Cliquez pour en savoir plus',
				],
				[
					'type'    => 'moyenne',
					'title'   => 'test 4eme',
					'content' => [
						'front' => 'Je suis un carte test',
						'back'  => 'c\'est une carte test',
					],
					'button'  => 'Ne pas cliquer /!\\',
				],
				[
					'type'     => 'grande',
					'title'    => 'grande carte',
					'content'  => [
						'front' => '',
						'back'  => '',
					],
					'button'   => 'Mystère',
					'vertical' => '',
				],
				[
					'type'    => 'moyenne',
					'title'   => 'test 5eme',
					'content' => [
						'front' => 'Je suis un carte test',
						'back'  => 'c\'est une carte test',
					],
					'button'  => 'Ne pas cliquer /!\\',
				],
				[
					'type'    => 'moyenne',
					'title'   => 'test 6eme',
					'content' => [
						'front' => 'Je suis un carte test',
						'back'  => 'c\'est une carte test',
					],
					'button'  => 'Ne pas cliquer /!\\',
				],
			
			];
			
			return $this->render( '@SixnappsCanvasTemplate/flip-cards.html.twig', [
				'flipCards' => $flipCards,
			] );
		}
	}
