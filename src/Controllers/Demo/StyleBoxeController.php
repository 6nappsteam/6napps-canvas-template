<?php
	
	namespace Sixnapps\CanvasTemplateBundle\Controllers\Demo;
	
	use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
	
	/**
	 * Class StyleBoxeController
	 *
	 * @package Sixnapps\CanvasTemplateBundle\Controllers
	 */
	class StyleBoxeController extends AbstractController
	{
		/**
		 * @return \Symfony\Component\HttpFoundation\Response
		 */
		public function styleBoxe()
		{
			
			$boxes = [
				[
					'type'      => 'native',
					'parameter' => 'successmsg',
					'icon'      => 'icon-paperplane',
					'close'     => TRUE,
				],
				[
					'type'      => 'native',
					'parameter' => 'errormsg',
					'content'   => "<strong>Bravo</strong> Tu es un champion !",
					'icon'      => 'icon-thumbs-up',
					'close'     => TRUE,
				],
				[
					'type'      => 'native',
					'parameter' => 'infomsg',
					'icon'      => 'icon-plane',
					'close'     => TRUE,
				],
				[
					'type'      => 'native',
					'parameter' => 'alertmsg',
					'icon'      => 'icon-smile',
					'close'     => '',
				],
				[
					'type'      => 'extended',
					'parameter' => 'errormsg',
					'title'     => 'Ceci est une alerte de niveau 4',
					'content'   => [
						'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sequi, adipisci.',
						'Placeat, et hic pariatur fugiat autem earum facere necessitatibus fuga.',
						'Aliquid, esse, perspiciatis iure rerum laudantium iste minima quas facere!',
						'Nam, ab, reiciendis magnam et odio inventore sapiente dolore vel.',
					],
				],
				[
					'type'      => 'bootstrap',
					'parameter' => 'alert-success',
					'content'   => '<strong>Well done!</strong> You successfully read this <a href="#" class="alert-link">important alert message</a>',
					'icon'      => 'icon-gift',
					'close'     => TRUE,
				],
			];
			
			return $this->render( '@SixnappsCanvasTemplate/style-boxe.html.twig', [
				'boxes' => $boxes,
			] );
		}
	}
