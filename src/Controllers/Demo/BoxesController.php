<?php
	
	namespace Sixnapps\CanvasTemplateBundle\Controllers\Demo;
	
	use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
	
	/**
	 * Class BoxesController
	 *
	 * @package Sixnapps\CanvasTemplateBundle\Controllers
	 */
	class BoxesController extends AbstractController
	{
		/**
		 * @return \Symfony\Component\HttpFoundation\Response
		 */
		public function boxes()
		{
			$boxes = [
				[
					'icon'        => 'icon-screen',
					'libelle'     => 'Responsive Layout',
					'description' => 'Powerful Layout with Responsive functionality that can be adapted to any screen size. Resize browser to view',
				],
				[
					'icon'        => 'icon-eye',
					'libelle'     => 'Retina Ready Graphics',
					'description' => 'Looks beautiful &amp; ultra-sharp on Retina Screen Displays. Retina Icons, Fonts &amp; all others graphics are optimized.',
					'parameters'  => [
						'fbox-outline',
						'fbox-dark',
					],
				],
				[
					'icon'        => 'icon-beaker',
					'libelle'     => 'Powerful Performance',
					'description' => 'Canvas includes tons of optimized code that are completely customizable and deliver unmatched fast performance',
				],
				[
					'icon'        => 'icon-share',
					'libelle'     => 'Socially Strong',
					'description' => 'Get ready to make your presence felt socially with some awesome included social features with Canvas.',
					'parameters'  => [
						'fbox-rounded',
					],
				],
				[
					'icon'        => 'icon-search3',
					'libelle'     => 'SEO Optimized',
					'description' => 'Canvas uses the best SEO optimization practises in order to give your website a Good Rank in Search Engines.',
					'parameters'  => [
						'fbox-rounded',
						'fbox-dark',
					],
				],
				[
					'icon'        => 'icon-hand-up',
					'libelle'     => 'Touch Enabled',
					'description' => 'All the Sliders &amp; Carousels are touch enabled to allow uniform experience across all major devices.',
					'parameters'  => [
						'fbox-rounded',
						'fbox-light',
					],
				],
				
				/*  Version Carousel  */
				
				[
					'carousel' => TRUE,
					'items'    => [
						[
							'icon'        => 'icon-hand-up',
							'libelle'     => 'Touch Enabled',
							'description' => 'All the Sliders &amp; Carousels are touch enabled to allow uniform experience across all major devices.',
						],
						[
							'icon'        => 'icon-eye',
							'libelle'     => 'Retina Ready Graphics',
							'description' => 'Looks beautiful &amp; ultra-sharp on Retina Screen Displays. Retina Icons, Fonts &amp; all others graphics are optimized.',
						],
						[
							'icon'        => 'icon-search3',
							'libelle'     => 'SEO Optimized',
							'description' => 'Canvas uses the best SEO optimization practises in order to give your website a Good Rank in Search Engines.',
						],
						[
							'icon'        => 'icon-share',
							'libelle'     => 'Socially Strong',
							'description' => 'Get ready to make your presence felt socially with some awesome included social features with Canvas.',
						],
					],
				],
			];
			
			return $this->render( '@SixnappsCanvasTemplate/boxes.html.twig', [
				'boxes' => $boxes,
			] );
		}
	}
