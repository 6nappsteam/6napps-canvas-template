<?php
/**
 * Created by PhpStorm.
 * User: dimitri
 * Date: 03/05/19
 * Time: 14:33
 */

namespace Sixnapps\CanvasTemplateBundle\Controllers\Demo;


use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class SidebarController extends AbstractController
{
    /**
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function sidebar()
    {
        return $this->render( '@SixnappsCanvasTemplate/sidebar.html.twig' );
    }
}