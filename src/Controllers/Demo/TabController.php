<?php
	
	namespace Sixnapps\CanvasTemplateBundle\Controllers\Demo;
	
	use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
	
	/**
	 * Class TabController
	 *
	 * @package Sixnapps\CanvasTemplateBundle\Controllers
	 */
	class TabController extends AbstractController
	{
		/**
		 * @return \Symfony\Component\HttpFoundation\Response
		 */
		public function tabs()
		{
			$tabs = [
				[
					'title'   => [
						'onglet 1',
						'onglet 2',
						'onglet 3',
					],
					'content' => [
						'contenu onglet 1',
						'contenu onglet 2',
						'contenu onglet 3',
					],
				],
				[
					'categorie'  => 'Avec bordure',
					'title'      => [
						'onglet 1',
						'onglet 2',
						'onglet 3',
					],
					'content'    => [
						'contenu onglet 1',
						'contenu onglet 2',
						'contenu onglet 3',
					],
					'parameters' => [
						'tabs-bordered',
					],
				],
				[
					'categorie'  => 'Style alterné',
					'title'      => [
						'onglet 1',
						'onglet 2',
						'onglet 3',
					],
					'content'    => [
						'contenu onglet 1',
						'contenu onglet 2',
						'contenu onglet 3',
					],
					'parameters' => [
						'tabs-alt',
					],
					'alternate'  => TRUE,
				],
				[
					'categorie'  => 'Avec bordure en haut',
					'title'      => [
						'onglet 1',
						'onglet 2',
						'onglet 3',
					],
					'content'    => [
						'contenu onglet 1',
						'contenu onglet 2',
						'contenu onglet 3',
					],
					'parameters' => [
						'tabs-alt',
						'tabs-tb',
					],
				],
				[
					'categorie'  => 'Avec bordure en bas',
					'title'      => [
						'onglet 1',
						'onglet 2',
						'onglet 3',
					],
					'content'    => [
						'contenu onglet 1',
						'contenu onglet 2',
						'contenu onglet 3',
					],
					'parameters' => [
						'tabs-bb',
					],
				],
				[
					'categorie'  => 'Onglet sur toute la longueur',
					'title'      => [
						'onglet 1',
						'onglet 2',
						'onglet 3',
					],
					'content'    => [
						'contenu onglet 1',
						'contenu onglet 2',
						'contenu onglet 3',
					],
					'parameters' => [
						'tabs-alt',
						'tabs-justify',
					],
				],
				[
					'categorie'  => 'Onglets responsive ( sous forme de liste ) ',
					'title'      => [
						'onglet 1',
						'onglet 2',
						'onglet 3',
					],
					'content'    => [
						'contenu onglet 1',
						'contenu onglet 2',
						'contenu onglet 3',
					],
					'parameters' => [
						'tabs-responsive',
					],
				],
				[
					'categorie'  => 'Sur le côté',
					'title'      => [
						'onglet 1',
						'onglet 2',
						'onglet 3',
					],
					'content'    => [
						'contenu onglet 1',
						'contenu onglet 2',
						'contenu onglet 3',
					],
					'parameters' => [
						'side-tabs',
						'responsive-tabs',
					],
				],
				[
					'categorie'  => 'Sur le côté avec bordure',
					'title'      => [
						'onglet 1',
						'onglet 2',
						'onglet 3',
					],
					'content'    => [
						'contenu onglet 1',
						'contenu onglet 2',
						'contenu onglet 3',
					],
					'parameters' => [
						'side-tabs',
						'tabs-bordered',
					],
				],
				[
					'categorie'  => 'Sur le côté avec navigation alterné',
					'title'      => [
						'onglet 1',
						'onglet 2',
						'onglet 3',
					],
					'content'    => [
						'contenu onglet 1',
						'contenu onglet 2',
						'contenu onglet 3',
					],
					'parameters' => [
						'side-tabs',
						'nobottommargin',
					],
					'alternate'  => TRUE,
				],
			];
			
			return $this->render( '@SixnappsCanvasTemplate/tabs.html.twig', [
				'tabs' => $tabs,
			] );
		}
	}
