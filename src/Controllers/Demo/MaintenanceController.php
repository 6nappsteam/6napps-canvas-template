<?php
	
	namespace Sixnapps\CanvasTemplateBundle\Controllers\Demo;
	
	use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
	
	/**
	 * Class MaintenanceController
	 *
	 * @package Sixnapps\CanvasTemplateBundle\Controllers
	 */
	class MaintenanceController extends AbstractController
	{
		/**
		 * @return \Symfony\Component\HttpFoundation\Response
		 */
		public function maintenance()
		{
			$maintenance = [
				'title'    => 'Site sous maintenance',
				'subtitle' => 'Veuillez vérifier dans quelques instants',
				'items'    => [
					[
						'icon'    => 'icon-warning-sign',
						'label'   => 'Pourquoi le site est e panne ?',
						'content' => 'Le site est probablement sous maintenance car nous avons besoin de l\'améliorer de façon drastique.',
					],
					[
						'icon'    => 'icon-time',
						'label'   => 'Quel est le temps d\'arrêt ?',
						'content' => 'Tout revient à la normal dans les 10-15 minutes généralement, mais cela dépend du problème',
					],
					[
						'icon'    => 'icon-email3',
						'label'   => 'Besoin de contacter le support ?',
						'content' => 'Vous pouvez nous contacter à <a href="mailto:info@canvas.com">info@canvas.com</a> si vous avez un problème urgent.',
					],
				],
			];
			
			return $this->render( '@SixnappsCanvasTemplate/maintenance.html.twig', [
				'maintenance' => $maintenance,
			] );
		}
	}
