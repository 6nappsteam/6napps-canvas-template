<?php
	
	namespace Sixnapps\CanvasTemplateBundle\Controllers\Demo;
	
	use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
	
	/**
	 * Class IndexController
	 *
	 * @package Sixnapps\CanvasTemplateBundle\Controllers
	 */
	class IndexController extends AbstractController
	{
		/**
		 * @return \Symfony\Component\HttpFoundation\Response
		 */
		public function index()
		{
			return $this->render( '@SixnappsCanvasTemplate/index.html.twig' );
		}
		
		
		/**
		 * @return \Symfony\Component\HttpFoundation\Response
		 */
		public function indexCorporate()
		{
			return $this->render( '@SixnappsCanvasTemplate/index-corporate.html.twig' );
		}
		
		
		/**
		 * @return \Symfony\Component\HttpFoundation\Response
		 */
		public function indexEmdf()
		{
			$header = [
				'logo' => [
					'standard'     => 'http://127.0.0.1:8005/build/assets/img/logo-wide-ecole.png',
					'standardDark' => '',
					'retina'       => '',
					'retinaDark'   => '',
					'alt'          => 'Ecole pour la mission',
				],
				'menu' => [
					'Accueil'             => [
						'url' => 'index.html',
					],
					'Les formations'      => [
						'url' => 'index.html',
					],
					'Nos intervenants'    => [
						'url' => 'index.html',
					],
					"A propos de l'École" => [
						'url' => 'index.html',
					],
					'Contactez-nous'      => [
						'url' => 'index.html',
					],
				
				],
				//				'cart' => [
				//
				//				],
				//				'search' => [
				//
				//				]
			];
			$title  = [
				'title'          => '<div id="inside_title" style="height:120px"></div>',
				'subTitle'       => '&nbsp;',
				'breadcrumb'     => '',
				'align'          => 'left',
				'img'     => 'http://127.0.0.1:8005/build/assets/img/header-bg-1.jpg',
				'topBottom'      => '0px -120px',
				'bottomTop'      => '0px 300px',
			];
			return $this->render( '@SixnappsCanvasTemplate/index-emdf.html.twig', [
				'header' => $header,
				'title'  => $title,
			] );
		}
	}
