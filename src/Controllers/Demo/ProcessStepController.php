<?php
	
	namespace Sixnapps\CanvasTemplateBundle\Controllers\Demo;
	
	use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
	
	/**
	 * Class ProcessStepController
	 *
	 * @package Sixnapps\CanvasTemplateBundle\Controllers
	 */
	class ProcessStepController extends AbstractController
	{
		
		/**
		 * @return \Symfony\Component\HttpFoundation\Response
		 */
		public function processStep()
		{
			return $this->render( '@SixnappsCanvasTemplate/process-steps.html.twig', [
				'steps' => [
					[
						'label'  => 'Review Cart',
						'active' => TRUE,
					],
					[
						'label' => 'Enter Shipping Info',
					],
					[
						'label' => 'Complete Payment',
					],
					[
						'label' => 'Order Complete',
					],
				],
			] );
		}
	}
	
