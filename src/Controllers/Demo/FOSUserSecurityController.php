<?php
	
	namespace Sixnapps\CanvasTemplateBundle\Controllers\Demo;
	
	use FOS\UserBundle\Controller\SecurityController;
	use Symfony\Component\HttpFoundation\Request;
	use Symfony\Component\HttpFoundation\Response;
	
	/**
	 * Class FOSUserSecurityController
	 *
	 * @package Sixnapps\CanvasTemplateBundle\Controllers\Demo
	 */
	class FOSUserSecurityController extends SecurityController
	{
		/**
		 * @param Request $request
		 *
		 * @return Response
		 */
		public function login( Request $request )
		{
			return $this->loginAction( $request );
		}
		
		public function renderLogin(array $data)
		{
			return $this->render('@SixnappsCanvasTemplate/security/FOSUserBundle/Security/demo-login.html.twig', $data);
		}

	}
