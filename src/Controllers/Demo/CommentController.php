<?php
	
	namespace Sixnapps\CanvasTemplateBundle\Controllers\Demo;
	
	use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
	
	/**
	 * Class BoxesController
	 *
	 * @package Sixnapps\CanvasTemplateBundle\Controllers
	 */
	class CommentController extends AbstractController
	{
		/**
		 * @return \Symfony\Component\HttpFoundation\Response
		 */
		public function comments()
		{
			return $this->render( '@SixnappsCanvasTemplate/comments.html.twig' );
		}
	}
