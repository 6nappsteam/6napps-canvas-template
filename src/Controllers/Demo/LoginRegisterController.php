<?php
	
	namespace Sixnapps\CanvasTemplateBundle\Controllers\Demo;
	
	use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
	
	/**
	 * Class ContactController
	 *
	 * @package Sixnapps\CanvasTemplateBundle\Controllers
	 */
	class LoginRegisterController extends AbstractController
	{
		/**
		 * @return \Symfony\Component\HttpFoundation\Response
		 */
		public function loginRegister1()
		{
			return $this->render( '@SixnappsCanvasTemplate/login-register-1.html.twig' );
		}
		
		
		/**
		 * @return \Symfony\Component\HttpFoundation\Response
		 */
		public function loginRegister2()
		{
			return $this->render( '@SixnappsCanvasTemplate/login-register-2.html.twig' );
		}
		
		
		/**
		 * @return \Symfony\Component\HttpFoundation\Response
		 */
		public function loginRegister3()
		{
			return $this->render( '@SixnappsCanvasTemplate/login-register-3.html.twig' );
		}
		
		
		/**
		 * @return \Symfony\Component\HttpFoundation\Response
		 */
		public function login1()
		{
			return $this->render( '@SixnappsCanvasTemplate/login-1.html.twig' );
		}
		
		
		/**
		 * @return \Symfony\Component\HttpFoundation\Response
		 */
		public function login2()
		{
			return $this->render( '@SixnappsCanvasTemplate/login-2.html.twig' );
		}
	}
