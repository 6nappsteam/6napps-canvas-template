<?php
	
	namespace Sixnapps\CanvasTemplateBundle\Controllers\Demo;
	
	use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
	
	/**
	 * Class PricingController
	 *
	 * @package Sixnapps\CanvasTemplateBundle\Controllers
	 */
	class PricingController extends AbstractController
	{
		/**
		 * @return \Symfony\Component\HttpFoundation\Response
		 */
		public function pricing()
		{
			$prices = [
				[
					'label'    => 'STARTER',
					'price'    => '<span class="price-unit">€</span>7<span class="price-tenure">/mo</span>',
					'features' => [
						'<strong>Full</strong> Access',
						'<i class="icon-code"></i> Source Files',
						'<strong>100</strong> User Accounts',
						'<strong>1 Year</strong> License',
						'Phone &amp; Email Support',
					],
					'action'   => [
						'type'  => 'danger',
						'href'  => '#',
						'label' => 'Sign Up',
					],
				],
				[
					'bestPrice' => TRUE,
					'label'     => 'PROFESSIONAL',
					'subLabel'  => 'MOST POPULAR',
					'price'     => '<span class="price-unit">€</span>12<span class="price-tenure">/mo</span>',
					'features'  => [
						'<strong>Full</strong> Access',
						'<i class="icon-code"></i> Source Files',
						'<strong>1000</strong> User Accounts',
						'<strong>2 Years</strong> License',
						'<i class="icon-star3"></i><i class="icon-star3"></i><i class="icon-star3"></i><i class="icon-star3"></i><i class="icon-star3"></i>',
					],
					'action'    => [
						'type'  => 'danger',
						'href'  => '#',
						'label' => 'Sign Up',
						'css'   => 'btn btn-danger btn-block btn-lg bgcolor border-color',
					],
				],
				[
					'label'    => 'BUSINESS',
					'price'    => '<span class="price-unit">€</span>19<span class="price-tenure">/mo</span>',
					'features' => [
						'<strong>Full</strong> Access',
						'<i class="icon-code"></i> Source Files',
						'<strong>500</strong> User Accounts',
						'<strong>3 Years</strong> License',
						'Phone &amp; Email Support',
					],
					'action'   => [
						'type'  => 'danger',
						'href'  => '#',
						'label' => 'Sign Up',
					],
				],
				[
					'label'    => 'ENTERPRISE',
					'price'    => '<span class="price-unit">€</span>29<span class="price-tenure">/mo</span>',
					'features' => [
						'<strong>Full</strong> Access',
						'<i class="icon-code"></i> Source Files',
						'<strong>1000</strong> User Accounts',
						'<strong>5 Years</strong> License',
						'Phone &amp; Email Support',
					],
					'action'   => [
						'type'  => 'danger',
						'href'  => '#',
						'label' => 'Sign Up',
					],
				],
			];
			
			return $this->render( '@SixnappsCanvasTemplate/pricing.html.twig', [
				'prices' => $prices,
			] );
		}
	}
