<?php
	
	namespace Sixnapps\CanvasTemplateBundle\Controllers\Demo;
	
	use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
	
	class RadiosSwitchesController extends AbstractController
	{
		
		public function radiosSwitches()
		{
			
			$radios = [
				'options'     => [
					[
						'label'   => 'Default',
						'options' => [],
					],
					[
						'label'   => 'Disabled',
						'options' => [
							'disabled',
						],
					],
					[
						'label'   => 'Readonly',
						'options' => [
							'readonly',
						],
					],
					[
						'label'   => 'Indeterminate',
						'options' => [
							'indeterminate',
						],
					],
					[
						'label'   => 'Inverse',
						'options' => [
							'inverse',
						],
					],
					[
						'label'   => 'Mini',
						'options' => [
							'mini',
						],
					],
				],
				'onColor'     => [
					[
						'options' => [],
						'onColor' => 'primary',
					],
					[
						'options' => [],
						'onColor' => 'info',
					],
					[
						'options' => [],
						'onColor' => 'success',
					],
					[
						'options' => [],
						'onColor' => 'warning',
					],
					[
						'options' => [],
						'onColor' => 'default',
					],
					[
						'options' => [],
						'onColor' => 'danger',
					],
					[
						'options' => [],
						'onColor' => 'themecolor',
					],
					[
						'options' => [],
						'onColor' => 'black',
					],
					[
						'options' => [],
						'onColor' => 'white',
					],
				],
				'offColor'    => [
					
					[
						'options'  => [ 'unchecked' ],
						'offColor' => 'primary',
					],
					[
						'options'  => [ 'unchecked' ],
						'offColor' => 'info',
					],
					[
						'options'  => [ 'unchecked' ],
						'offColor' => 'success',
					],
					[
						'options'  => [ 'unchecked' ],
						'offColor' => 'warning',
					],
					[
						'options'  => [ 'unchecked' ],
						'offColor' => 'default',
					],
					[
						'options'  => [ 'unchecked' ],
						'offColor' => 'danger',
					],
					[
						'options'  => [ 'unchecked' ],
						'offColor' => 'themecolor',
					],
					[
						'options'  => [ 'unchecked' ],
						'offColor' => 'black',
					],
					[
						'options'  => [ 'unchecked' ],
						'offColor' => 'white',
					],
				],
				'customText'  => [
					[
						'options'  => [],
						'onText'   => 'Yes',
						'offText'  => 'No',
						'onColor'  => 'info',
						'offColor' => 'danger',
					],
					[
						'options'  => [],
						'onText'   => 'Enable',
						'onColor'  => NULL,
						'offText'  => 'Disable',
						'offColor' => NULL,
					],
					[
						'options'  => [],
						'onText'   => 'Online',
						'offText'  => 'Offline',
						'onColor'  => 'danger',
						'offColor' => 'default',
					],
					[
						'options'  => [],
						'onText'   => 'abc',
						'offText'  => 'xyz',
						'onColor'  => 'white',
						'offColor' => 'black',
					],
				],
				'customIcon'  => [
					[
						'options'  => [],
						'onText'   => '<i class=\'icon-thumbs-up\'></i>',
						'offText'  => '<i class=\'icon-thumbs-down\'></i>',
						'onColor'  => NULL,
						'offColor' => NULL,
					],
					[
						'options'  => [],
						'onText'   => '<i class=\'icon-smile\'></i>',
						'onColor'  => 'info',
						'offText'  => '<i class=\'icon-frown\'></i>',
						'offColor' => 'danger',
					],
					[
						'options'  => [],
						'onText'   => '<i class=\'icon-lock3\'></i>',
						'onColor'  => 'default',
						'offText'  => '<i class=\'icon-unlock\'></i>',
						'offColor' => 'danger',
					],
					[
						'options'  => [],
						'onText'   => '<i class=\'icon-line2-envelope-open\'></i>',
						'onColor'  => 'success',
						'offText'  => '<i class=\'icon-line2-envelope\'></i>',
						'offColor' => 'danger',
					],
					[
						'options'  => [],
						'onText'   => '<i class=\'icon-line-circle-check\'></i>',
						'onColor'  => 'white',
						'offText'  => '<i class=\'icon-line-circle-cross\'></i>',
						'offColor' => 'black',
					],
					[
						'options'  => [],
						'onText'   => '<i class=\'icon-line-check\'></i>',
						'onColor'  => 'black',
						'offText'  => '<i class=\'icon-line-cross\'></i>',
						'offColor' => 'white',
					],
				],
				'customWidth' => [
					[
						'options'  => [],
						'onText'   => '20',
						'offText'  => '20',
						'width'    => 20,
						'onColor'  => NULL,
						'offColor' => NULL,
					],
					[
						'options'  => [],
						'onText'   => '50',
						'offText'  => '50',
						'width'    => 50,
						'onColor'  => NULL,
						'offColor' => NULL,
					],
					[
						'options'  => [],
						'onText'   => '100',
						'offText'  => '100',
						'width'    => 100,
						'onColor'  => NULL,
						'offColor' => NULL,
					],
				],
			
			];
			
			$switches = [
				'round' => [
					[
						'round' => TRUE,
						'size'  => 'mini',
					],
					[
						'round' => TRUE,
						'size'  => 'default',
					],
					[
						'round' => TRUE,
						'size'  => 'large',
					],
					[
						'round' => TRUE,
						'size'  => 'xlarge',
					],
				],
				'flat'  => [
					[
						'flat' => TRUE,
						'size' => 'mini',
					],
					[
						'flat' => TRUE,
						'size' => 'default',
					],
					[
						'flat' => TRUE,
						'size' => 'large',
					],
					[
						'flat' => TRUE,
						'size' => 'xlarge',
					],
				],
			];
			
			$modal = [
				'dataTarget'  => 'myModal', // id de la modale
				'labelledby'  => 'myModalLabel',
				'size'        => 'default', // default, small, large
				'buttonLabel' => 'Launch modal',
				'buttonClass' => 'btn btn-primary btn-lg',
				'headTitle'   => 'Switcher inside Modal',
			];
			
			return $this->render( '@SixnappsCanvasTemplate/radios-switches.html.twig', [
				'radios'   => $radios,
				'switches' => $switches,
				'modal'    => $modal,
			] );
			
		}
	}
