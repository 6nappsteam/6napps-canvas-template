<?php
	
	namespace Sixnapps\CanvasTemplateBundle\Controllers\Demo;
	
	use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
	
	/**
	 * Class CareersController
	 *
	 * @package Sixnapps\CanvasTemplateBundle\Controllers
	 */
	class CareerController extends AbstractController
	{
		/**
		 * @return \Symfony\Component\HttpFoundation\Response
		 */
		public function career()
		{
			$careers = [
				
				'form'  => [
					'action' => '',
				],
				'items' => [
					[
						'title'        => 'Junior Symfony Developer',
						'description'  => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nemo, natus voluptatibus adipisci porro magni dolore eos eius ducimus corporis quos perspiciatis quis iste, vitae autem libero ullam omnis cupiditate quam.',
						'requirements' => [
							'B.Tech./ B.E / MCA degree in Computer Science, Engineering or a related stream.',
							'3+ years of software development experience.',
							'3+ years of Python / Java development projects experience.',
							'Minimum of 4 live project roll outs.',
							'Experience with third-party libraries and APIs.',
							'In depth understanding and experience  of either SDLC or PDLC.',
							'Good Communication Skills',
							'Team Player',
						],
						'expects'      => [
							'Design and build applications/ components using open source technology.',
							'Taking complete ownership of the deliveries assigned.',
							'Collaborate with cross-functional teams to define, design, and ship new features.',
							'Work with outside data sources and API\'s.',
							'Unit-test code for robustness, including edge cases, usability, and general reliability.',
							'Work on bug fixing and improving application performance.',
						],
						'profil'       => 'You\'ll be familiar with agile practices and have a highly technical background. <br> Comfortable discussing detailed technical aspects of system design and implementation, whilst remaining business driven. <br> With 5+ years of systems analysis, technical analysis or business analysis experience, you\'ll have an expansive toolkit of communication techniques to enable shared, deep understanding of financial and technical concepts by diverse stakeholders with varying backgrounds and needs.<br> In addition, you will have exposure to financial systems or accounting knowledge.',
					],
					[
						'title'        => 'Design Analyst',
						'description'  => 'Repudiandae quasi perspiciatis ea placeat nobis asperiores quod fuga ipsa facere enim ipsum expedita debitis, sit quia adipisci deserunt vitae hic obcaecati voluptates rerum nihil.',
						'requirements' => [
							'B.Tech./ B.E / MCA degree in Computer Science, Engineering or a related stream.',
							'3+ years of software development experience.',
							'3+ years of Python / Java development projects experience.',
							'Minimum of 4 live project roll outs.',
							'Experience with third-party libraries and APIs.',
							'In depth understanding and experience  of either SDLC or PDLC.',
							'Good Communication Skills',
							'Team Player',
						],
						'expects'      => [
							'Design and build applications/ components using open source technology.',
							'Taking complete ownership of the deliveries assigned.',
							'Collaborate with cross-functional teams to define, design, and ship new features.',
							'Work with outside data sources and API\'s.',
							'Unit-test code for robustness, including edge cases, usability, and general reliability.',
							'Work on bug fixing and improving application performance.',
						],
						'profil'       => 'You\'ll be familiar with agile practices and have a highly technical background. <br> Comfortable discussing detailed technical aspects of system design and implementation, whilst remaining business driven. <br> With 5+ years of systems analysis, technical analysis or business analysis experience, you\'ll have an expansive toolkit of communication techniques to enable shared, deep understanding of financial and technical concepts by diverse stakeholders with varying backgrounds and needs.<br> In addition, you will have exposure to financial systems or accounting knowledge.',
					],
				],
			];
			
			return $this->render( '@SixnappsCanvasTemplate/career.html.twig', [
				'careers' => $careers,
			] );
		}
	}
