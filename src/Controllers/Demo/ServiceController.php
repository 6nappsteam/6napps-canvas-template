<?php
	
	namespace Sixnapps\CanvasTemplateBundle\Controllers\Demo;
	
	use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
	
	/**
	 * Class SecutiryController
	 *
	 * @package Sixnapps\CanvasTemplateBundle\Controllers
	 */
	class ServiceController extends AbstractController
	{
		/**
		 * @return \Symfony\Component\HttpFoundation\Response
		 */
		public function serviceLayout1()
		{
			return $this->render( '@SixnappsCanvasTemplate/service-layout-1.html.twig' );
		}
	}
