<?php
	
	namespace Sixnapps\CanvasTemplateBundle\Controllers\Demo;
	
	use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
	
	class ModalPopoversController extends AbstractController
	{
		
		public function modalPopovers()
		{
			$modals = [
				[
					'dataTarget'  => 'myModal', // id de la modale
					'labelledby'  => 'myModalLabel',
					'size'        => 'default', // default, small, large
					'buttonLabel' => 'Launch modal',
					'buttonClass' => 'btn btn-primary btn-lg',
					'headTitle'   => 'Modal Heading',
					'footer'      => [
						'closeLabel'       => 'Close',
						'buttonCloseClass' => 'btn btn-secondary',
						'actions'          => [
							[
								'buttonLabel' => 'Save Change',
								'type'        => 'button', // button or a,
								'buttonClass' => 'btn btn-primary',
							],
						],
					],
					'body'        => '
											<h4>Text in a modal</h4>
											<p>Duis mollis, est non commodo luctus, nisi erat porttitor ligula.</p>

											<h4>Popover in a modal</h4>
											<p>This <a href="#" role="button" class="btn btn-secondary" data-toggle="popover" title="" data-content="And here\'s some amazing content. It\'s very engaging. right?" data-original-title="A Title">button</a> should trigger a popover on click.</p>

											<h4>Tooltips in a modal</h4>
											<p><a href="#" data-toggle="tooltip" title="" data-original-title="Tooltip">This link</a> and <a href="#" data-toggle="tooltip" title="" data-original-title="Tooltip">that link</a> should have tooltips on hover.</p>
											<hr>
											<h4>Overflowing text to show scroll behavior</h4>
											<p>Cras mattis consectetur purus sit amet fermentum. Cras justo odio, dapibus ac facilisis in, egestas eget quam. Morbi leo risus, porta ac consectetur ac, vestibulum at eros.</p>
											<p>Praesent commodo cursus magna, vel scelerisque nisl consectetur et. Vivamus sagittis lacus vel augue laoreet rutrum faucibus dolor auctor.</p>
											<p>Aenean lacinia bibendum nulla sed consectetur. Praesent commodo cursus magna, vel scelerisque nisl consectetur et. Donec sed odio dui. Donec ullamcorper nulla non metus auctor fringilla.</p>
											<p>Cras mattis consectetur purus sit amet fermentum. Cras justo odio, dapibus ac facilisis in, egestas eget quam. Morbi leo risus, porta ac consectetur ac, vestibulum at eros.</p>
											<p>Praesent commodo cursus magna, vel scelerisque nisl consectetur et. Vivamus sagittis lacus vel augue laoreet rutrum faucibus dolor auctor.</p>
											<p>Aenean lacinia bibendum nulla sed consectetur. Praesent commodo cursus magna, vel scelerisque nisl consectetur et. Donec sed odio dui. Donec ullamcorper nulla non metus auctor fringilla.</p>
											<p>Cras mattis consectetur purus sit amet fermentum. Cras justo odio, dapibus ac facilisis in, egestas eget quam. Morbi leo risus, porta ac consectetur ac, vestibulum at eros.</p>
											<p>Praesent commodo cursus magna, vel scelerisque nisl consectetur et. Vivamus sagittis lacus vel augue laoreet rutrum faucibus dolor auctor.</p>
											<p class="nobottommargin">Aenean lacinia bibendum nulla sed consectetur. Praesent commodo cursus magna, vel scelerisque nisl consectetur et. Donec sed odio dui. Donec ullamcorper nulla non metus auctor fringilla.</p>
										',
				
				],
				[
					'dataTarget'  => 'myModal2', // id de la modale
					'labelledby'  => 'myModalLabel2',
					'size'        => 'large', // default, small, large
					'buttonLabel' => 'Large modal',
					'buttonClass' => 'btn btn-primary',
					'headTitle'   => 'Modal Heading',
					'body'        => '
											<p>Praesent commodo cursus magna, vel scelerisque nisl consectetur et. Vivamus sagittis lacus vel augue laoreet rutrum faucibus dolor auctor.</p>
											<p>Aenean lacinia bibendum nulla sed consectetur. Praesent commodo cursus magna, vel scelerisque nisl consectetur et. Donec sed odio dui. Donec ullamcorper nulla non metus auctor fringilla.</p>
											<p>Cras mattis consectetur purus sit amet fermentum. Cras justo odio, dapibus ac facilisis in, egestas eget quam. Morbi leo risus, porta ac consectetur ac, vestibulum at eros.</p>
											<p>Praesent commodo cursus magna, vel scelerisque nisl consectetur et. Vivamus sagittis lacus vel augue laoreet rutrum faucibus dolor auctor.</p>
											<p class="nobottommargin">Aenean lacinia bibendum nulla sed consectetur. Praesent commodo cursus magna, vel scelerisque nisl consectetur et. Donec sed odio dui. Donec ullamcorper nulla non metus auctor fringilla.</p>
										',
				],
				[
					'dataTarget'  => 'myModal3', // id de la modale
					'labelledby'  => 'myModalLabel3',
					'size'        => 'small', // default, small, large
					'buttonLabel' => 'Small modal',
					'buttonClass' => 'btn btn-primary',
					'headTitle'   => 'Modal Heading',
					'body'        => '
											<p>Praesent commodo cursus magna, vel scelerisque nisl consectetur et. Vivamus sagittis lacus vel augue laoreet rutrum faucibus dolor auctor.</p>
											<p>Aenean lacinia bibendum nulla sed consectetur. Praesent commodo cursus magna, vel scelerisque nisl consectetur et. Donec sed odio dui. Donec ullamcorper nulla non metus auctor fringilla.</p>
											<p>Cras mattis consectetur purus sit amet fermentum. Cras justo odio, dapibus ac facilisis in, egestas eget quam. Morbi leo risus, porta ac consectetur ac, vestibulum at eros.</p>
											<p>Praesent commodo cursus magna, vel scelerisque nisl consectetur et. Vivamus sagittis lacus vel augue laoreet rutrum faucibus dolor auctor.</p>
											<p class="nobottommargin">Aenean lacinia bibendum nulla sed consectetur. Praesent commodo cursus magna, vel scelerisque nisl consectetur et. Donec sed odio dui. Donec ullamcorper nulla non metus auctor fringilla.</p>
										',
				],
			];
			
			$popovers = [
				[
					'buttonClass' => 'btn btn-secondary',
					'buttonLabel' => 'Popover on right',
					'placement'   => 'right', // top, left, bottom, right
					'content'     => 'Vivamus sagittis lacus vel augue laoreet rutrum faucibus.',
				],
				[
					'buttonClass' => 'btn btn-secondary',
					'buttonLabel' => 'Popover on top',
					'placement'   => 'top', // top, left, bottom, right
					'content'     => 'Vivamus sagittis lacus vel augue laoreet rutrum faucibus.',
				],
				[
					'buttonClass' => 'btn btn-secondary',
					'buttonLabel' => 'Popover on bottom',
					'placement'   => 'bottom', // top, left, bottom, right
					'content'     => 'Vivamus sagittis lacus vel augue laoreet rutrum faucibus.',
				],
				[
					'buttonClass' => 'btn btn-secondary',
					'buttonLabel' => 'Popover on left',
					'placement'   => 'left', // top, left, bottom, right
					'content'     => 'Vivamus sagittis lacus vel augue laoreet rutrum faucibus.',
				],
			];
			
			$tooltips = [
				[
					'element'      => 'button', // button, a, span
					'elementClass' => 'btn btn-secondary',
					'elementLabel' => 'Tooltip on right',
					'placement'    => 'right', // top, left, bottom, right
					'content'      => 'Tooltip on right',
				],
				[
					'element'      => 'button', // button, a, span
					'elementClass' => 'btn btn-secondary',
					'elementLabel' => 'Tooltip on top',
					'placement'    => 'top', // top, left, bottom, right
					'content'      => 'Tooltip on top',
				],
				[
					'element'      => 'button', // button, a, span
					'elementClass' => 'btn btn-secondary',
					'elementLabel' => 'Tooltip on bottom',
					'placement'    => 'bottom', // top, left, bottom, right
					'content'      => 'Tooltip on bottom',
				],
				[
					'element'      => 'button', // button, a, span
					'elementClass' => 'btn btn-secondary',
					'elementLabel' => 'Tooltip on left',
					'placement'    => 'left', // top, left, bottom, right
					'content'      => 'Tooltip on left',
				],
				[
					'element'      => 'a', // button, a, span
					'elementClass' => 'px-2',
					'elementLabel' => 'Tooltip on top',
					'placement'    => 'top', // top, left, bottom, right
					'content'      => 'Tooltip on top',
					'elementHref'  => '#',
				],
				[
					'element'      => 'span', // button, a, span
					'elementClass' => 'px-2',
					'elementLabel' => 'Tooltip on top',
					'placement'    => 'top', // top, left, bottom, right
					'content'      => 'Tooltip on top',
				],
			];
			
			$notifications = [
				[
					'element'      => 'a', // a, button
					'elementHref'  => '#', // optionnel: string ou null
					'elementClass' => 'btn btn-info',
					'elementLabel' => 'Show Info',
					'position'     => 'top-right', // top-right, top-left, bottom-right, bottom-left, top-full-width, bottom-full-width
					'notifyType'   => 'info', //info, error, success, warning
					'message'      => '<i class=icon-info-sign></i> Welcome to Canvas Demo!',
				],
				[
					'element'      => 'a', // a, button
					'elementHref'  => '#', // optionnel: string ou null
					'elementClass' => 'btn btn-danger',
					'elementLabel' => 'Show Info',
					'position'     => 'top-right', // top-right, top-left, bottom-right, bottom-left, top-full-width, bottom-full-width
					'notifyType'   => 'error', //info, error, success, warning
					'message'      => '<i class=icon-info-sign></i> Welcome to Canvas Demo!',
				],
				[
					'element'      => 'a', // a, button
					'elementHref'  => '#', // optionnel: string ou null
					'elementClass' => 'btn btn-success',
					'elementLabel' => 'Show Info',
					'position'     => 'top-right', // top-right, top-left, bottom-right, bottom-left, top-full-width, bottom-full-width
					'notifyType'   => 'success', //info, error, success, warning
					'message'      => '<i class=icon-info-sign></i> Welcome to Canvas Demo!',
				],
				[
					'element'      => 'a', // a, button
					'elementHref'  => '#', // optionnel: string ou null
					'elementClass' => 'btn btn-warning',
					'elementLabel' => 'Show Info',
					'position'     => 'top-right', // top-right, top-left, bottom-right, bottom-left, top-full-width, bottom-full-width
					'notifyType'   => 'warning', //info, error, success, warning
					'message'      => '<i class=icon-info-sign></i> Welcome to Canvas Demo!',
				],
				[
					'element'      => 'a', // a, button
					'elementHref'  => '#', // optionnel: string ou null
					'elementClass' => 'btn btn-info',
					'elementLabel' => 'Top right',
					'position'     => 'top-right', // top-right, top-left, bottom-right, bottom-left, top-full-width, bottom-full-width
					'notifyType'   => 'info', //info, error, success, warning
					'message'      => '<i class=icon-info-sign></i> Welcome to Canvas Demo!',
				],
				[
					'element'      => 'a', // a, button
					'elementHref'  => '#', // optionnel: string ou null
					'elementClass' => 'btn btn-info',
					'elementLabel' => 'Top Left',
					'position'     => 'top-left', // top-right, top-left, bottom-right, bottom-left, top-full-width, bottom-full-width
					'notifyType'   => 'info', //info, error, success, warning
					'message'      => '<i class=icon-info-sign></i> Welcome to Canvas Demo!',
				],
				[
					'element'      => 'a', // a, button
					'elementHref'  => '#', // optionnel: string ou null
					'elementClass' => 'btn btn-info',
					'elementLabel' => 'Bottom right',
					'position'     => 'bottom-right', // top-right, top-left, bottom-right, bottom-left, top-full-width, bottom-full-width
					'notifyType'   => 'info', //info, error, success, warning
					'message'      => '<i class=icon-info-sign></i> Welcome to Canvas Demo!',
				],
				[
					'element'      => 'a', // a, button
					'elementHref'  => '#', // optionnel: string ou null
					'elementClass' => 'btn btn-info',
					'elementLabel' => 'Bottom Left',
					'position'     => 'bottom-left', // top-right, top-left, bottom-right, bottom-left, top-full-width, bottom-full-width
					'notifyType'   => 'info', //info, error, success, warning
					'message'      => '<i class=icon-info-sign></i> Welcome to Canvas Demo!',
				],
				
				[
					'element'      => 'a', // a, button
					'elementHref'  => '#', // optionnel: string ou null
					'elementClass' => 'btn btn-info',
					'elementLabel' => 'Top full width',
					'position'     => 'top-full-width', // top-right, top-left, bottom-right, bottom-left, top-full-width, bottom-full-width
					'notifyType'   => 'info', //info, error, success, warning
					'message'      => '<i class=icon-info-sign></i> Welcome to Canvas Demo!',
				],
				[
					'element'      => 'button', // a, button
					'elementClass' => 'btn btn-info',
					'elementLabel' => 'Bottom Full Width',
					'position'     => 'bottom-full-width', // top-right, top-left, bottom-right, bottom-left, top-full-width, bottom-full-width
					'notifyType'   => 'info', //info, error, success, warning
					'message'      => '<i class=icon-info-sign></i> Welcome to Canvas Demo!',
				],
			
			];
			
			return $this->render( '@SixnappsCanvasTemplate/modal-popovers.html.twig', [
				'modals'        => $modals,
				'popovers'      => $popovers,
				'tooltips'      => $tooltips,
				'notifications' => $notifications,
			] );
		}
	}
