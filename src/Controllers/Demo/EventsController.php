<?php
	
	namespace Sixnapps\CanvasTemplateBundle\Controllers\Demo;
	
	use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
	
	/**
	 * Class EventsController
	 *
	 * @package Sixnapps\CanvasTemplateBundle\Controllers
	 */
	class EventsController extends AbstractController
	{
		/**
		 * @return \Symfony\Component\HttpFoundation\Response
		 * @throws \Exception
		 */
		public function listFullwidth()
		{
			$events = [
				[
					'image'   => 'canvas/images/events/thumbs/1.jpg',
					'title'   => 'Inventore voluptates velit totam ipsa tenetur',
					'date'    => new \DateTime( '04/10/2019' ),
					'metas'   => [
						'badge'    => [
							'type'  => 'warning',
							'label' => 'Private',
						],
						'hours'    => '11:00 - 19:00',
						'location' => 'Melbourne, Australia',
					],
					'content' => 'orem ipsum dolor sit amet, consectetur adipisicing elit. Ratione, voluptatem, dolorem animi nisi
				   autem blanditiis enim culpa reiciendis et explicabo tenetur voluptate rerum molestiae eaque possimus
				   exercitationem eligendi fuga.',
					'actions' => [
						[
							'type'  => 'secondary',
							'href'  => '#',
							'label' => 'Buy Tickets',
						],
						[
							'type'  => 'danger',
							'href'  => '#',
							'label' => 'Read More',
						],
					],
				],
			
			];
			return $this->render( '@SixnappsCanvasTemplate/events-list-fullwidth.html.twig', [
				'events' => $events,
			] );
		}
	}
