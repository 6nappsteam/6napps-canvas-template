<?php
	
	namespace Sixnapps\CanvasTemplateBundle\Controllers\Demo;
	
	use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
	
	/**
	 * Class CarouselController
	 *
	 * @package Sixnapps\CanvasTemplateBundle\Controllers
	 */
	class CarouselController extends AbstractController
	{
		/**
		 * @return \Symfony\Component\HttpFoundation\Response
		 */
		public function carousel()
		{
			$carousels = [
				[
					'type'   => 'image',
					'images' => [
						'portfolio/4/4-1.jpg',
						'portfolio/4/6-1.jpg',
						'portfolio/4/6-2.jpg',
						'portfolio/4/6-3.jpg',
						'portfolio/4/9-1.jpg',
						'portfolio/4/9-2.jpg',
						'portfolio/4/12-1.jpg',
					],
				],
				[
					'type'  => 'event',
					'items' => [
						// First item test
						[
							[
								'title'     => 'Title 1',
								'thumbnail' => 'events/thumbs/1.jpg',
								'day'       => '10',
								'month'     => 'Apr',
								'start'     => '12:00',
								'end'       => '19:00',
								'city'      => 'London',
								'statut'    => 'Private',
								'badge'     => 'warning',
								'buttons'   => [
									[
										'name'  => 'Buy tickets',
										'color' => 'secondary',
										'link'  => '#',
									],
									[
										'name'  => 'Read more',
										'color' => 'danger',
										'link'  => '#',
									],
								],
							],
							[
								'title'     => 'Title 2',
								'thumbnail' => 'events/thumbs/2.jpg',
								'day'       => '16',
								'month'     => 'Apr',
								'start'     => '11:00',
								'end'       => '19:00',
								'city'      => 'Melbourne',
								'statut'    => 'Urgent',
								'badge'     => 'danger',
								'buttons'   => [
									[
										'name'  => 'RSVP',
										'color' => 'primary',
										'link'  => '#',
									],
									[
										'name'  => 'Read more',
										'color' => 'danger',
										'link'  => '#',
									],
								],
							],
						],
						// Second item test
						[
							[
								'title'     => 'Title 3',
								'thumbnail' => 'events/thumbs/3.jpg',
								'day'       => '3',
								'month'     => 'May',
								'start'     => '11:00',
								'end'       => '19:00',
								'city'      => 'New York',
								'statut'    => 'Public',
								'badge'     => 'primary',
								'buttons'   => [
									[
										'name'  => 'Buy tickets',
										'color' => 'secondary',
										'link'  => '#',
									],
									[
										'name'  => 'Read more',
										'color' => 'danger',
										'link'  => '#',
									],
								],
							],
							[
								'title'     => 'Title 4',
								'thumbnail' => 'events/thumbs/4.jpg',
								'day'       => '16',
								'month'     => 'Jun',
								'start'     => '11:00',
								'end'       => '19:00',
								'city'      => 'Los Angeles',
								'statut'    => 'Private',
								'badge'     => 'warning',
								'buttons'   => [
									[
										'name'  => 'RSVP',
										'color' => 'primary',
										'link'  => '#',
									],
									[
										'name'  => 'Read more',
										'color' => 'danger',
										'link'  => '#',
									],
								],
							],
						],
					
					],
				],
				[
					'type'  => 'portfolio',
					'items' => [
						[
							'data'      => 'image',
							'thumbnail' => 'portfolio/4/1.jpg',
							'full'      => 'portfolio/full/1.jpg',
							'title'     => 'Open Imagination',
							'link'      => '#',
						],
						[
							'data'      => 'image',
							'thumbnail' => 'portfolio/4/2.jpg',
							'full'      => 'portfolio/full/2.jpg',
							'title'     => 'Locked Steel Gate',
							'link'      => '#',
						],
						[
							'data'      => 'image',
							'thumbnail' => 'portfolio/4/5.jpg',
							'full'      => 'portfolio/full/5.jpg',
							'title'     => 'Mac Sunglasses',
							'link'      => '#',
						],
						[
							'data'      => 'image',
							'thumbnail' => 'portfolio/4/8.jpg',
							'full'      => 'portfolio/full/8.jpg',
							'title'     => 'Console Activity',
							'link'      => '#',
						],
						[
							'data'      => 'image',
							'thumbnail' => 'portfolio/4/11.jpg',
							'full'      => 'portfolio/full/11.jpg',
							'title'     => 'Backpack Contents',
							'link'      => '#',
						],
						
						// IFRAME
						[
							'data'      => 'iframe',
							'thumbnail' => 'portfolio/4/3.jpg',
							'full'      => 'http://vimeo.com/89396394',
							'title'     => 'Sunset Bulb Glow',
							'link'      => '#',
						],
						[
							'data'      => 'iframe',
							'thumbnail' => 'portfolio/4/7.jpg',
							'full'      => 'http://www.youtube.com/watch?v=kuceVNBTJio',
							'title'     => 'Study Table',
							'link'      => '#',
						],
						[
							'data'      => 'iframe',
							'thumbnail' => 'portfolio/4/10.jpg',
							'full'      => 'http://vimeo.com/91973305',
							'title'     => 'Workspace Stuff',
							'link'      => '#',
						],
					
					],
				],
				[
					'type'  => 'portfolio-full',
					'items' => [
						[
							'data'  => 'simple',
							'image' => [
								'thumbnail' => 'portfolio/4/1.jpg',
								'full'      => 'portfolio/full/1.jpg',
							],
						],
						[
							'data'  => 'gallery',
							'image' => [
								'thumbnail' => 'portfolio/4/4.jpg',
								'full'      => [
									'portfolio/full/4.jpg',
									'portfolio/full/4-1.jpg',
								],
							
							],
							'title' => 'Morning Dew',
							'link'  => '#',
						],
						[
							'data'  => 'iframe',
							'image' => 'portfolio/4/3.jpg',
							'video' => 'http://vimeo.com/89396394',
							'title' => 'Video over the top',
							'link'  => '#',
						],
					],
				],
			];
			
			return $this->render( '@SixnappsCanvasTemplate/carousel.html.twig', [
				'carousels' => $carousels,
			] );
		}
	}
