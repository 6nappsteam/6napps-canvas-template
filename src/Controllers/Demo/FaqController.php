<?php
	
	namespace Sixnapps\CanvasTemplateBundle\Controllers\Demo;
	
	use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
	
	/**
	 * Class FaqController
	 *
	 * @package Sixnapps\CanvasTemplateBundle\Controllers
	 */
	class FaqController extends AbstractController
	{
		/**
		 * @return \Symfony\Component\HttpFoundation\Response
		 */
		public function faq()
		{
			$faq = [
				[
					
					'onglets'   => [
						[
							'libelle' => 'Marketplace',
							'class'   => 'marketplace',
						],
						[
							'libelle' => 'Authors',
							'class'   => 'authors',
						],
						[
							'libelle' => 'Legal',
							'class'   => 'legal',
						],
						[
							'libelle' => 'Item Discussion',
							'class'   => 'itemdiscussion',
						],
						[
							'libelle' => 'Affiliates',
							'class'   => 'affiliates',
						],
						[
							'libelle' => 'Miscellaneous',
							'class'   => 'miscellaneous',
						],
					
					],
					'questions' => [
						[
							'libelle' => 'How do I become a God ?',
							'reponse' => '',
							'icon'    => 'icon-question-sign',
							'class'   => 'marketplace',
						],
						[
							'libelle' => 'Helpful Resources for Authors',
							'reponse' => '',
							'icon'    => 'icon-comments-alt',
							'class'   => 'miscellaneous',
						],
						[
							'libelle' => 'Can I offer my items for free on a promotional basis?',
							'reponse' => '',
							'icon'    => 'icon-download-alt',
							'class'   => 'legal',
						],
						[
							'libelle' => 'An Introduction to the Marketplaces for Authors',
							'reponse' => '',
							'icon'    => 'icon-ok',
							'class'   => 'authors',
						],
						[
							'libelle' => 'What Images, Videos, Code or Music Can I Use in my Items?',
							'reponse' => '',
							'icon'    => 'icon-picture',
							'class'   => 'itemdiscussion',
						],
						[
							'libelle' => 'Tips for Increasing Your Referral Income',
							'reponse' => '',
							'icon'    => 'icon-bar-chart',
							'class'   => 'affiliates',
						],
					],
				],
			];
			
			return $this->render( '@SixnappsCanvasTemplate/faq.html.twig', [
				'faq' => $faq,
			] );
		}
	}
