<?php
	/**
	 * Created by PhpStorm.
	 * User: virginie
	 * Date: 20/02/19
	 * Time: 10:06
	 */
	
	namespace Sixnapps\CanvasTemplateBundle\Controllers\Demo;
	
	use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
	
	class VideoController extends AbstractController
	{
		public function videos()
		{
			return $this->render( '@SixnappsCanvasTemplate/videos.html.twig' );
		}
		
	}
