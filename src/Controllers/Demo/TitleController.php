<?php
	
	namespace Sixnapps\CanvasTemplateBundle\Controllers\Demo;
	
	use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
	
	/**
	 * Class TitleController
	 *
	 * @package Sixnapps\CanvasTemplateBundle\Controllers
	 */
	class TitleController extends AbstractController
	{
		
		/**
		 * @return \Symfony\Component\HttpFoundation\Response
		 */
		public function title()
		{
			$title      = 'Test';
			$subTitle   = 'Foo';
			$breadcrumb = [ [ 'label' => 'Home', 'link' => '#' ], [ 'label' => 'Library', 'link' => '#' ], [ 'label' => 'Data', 'link' => '#' ] ];
			
			return $this->render( '@SixnappsCanvasTemplate/titles.html.twig', [
				'titleDatasD'     => [ 'title' => $title, 'subTitle' => $subTitle, 'breadcrumb' => $breadcrumb ],
				'titleDatasDCD'   => [ 'title' => $title, 'subTitle' => $subTitle, 'breadcrumb' => $breadcrumb, 'customDark' => TRUE, 'titleBgColor' => "red", 'textBgColor' => 'blue' ],
				'titleDatasDCDC'   => [ 'title' => $title, 'subTitle' => $subTitle, 'breadcrumb' => $breadcrumb, 'customDark' => TRUE, 'align' => 'right', 'titleBgColor' => "red", 'textBgColor' => 'blue', 'cover' => 'rgba(255, 83, 113, 0.8)' ],
				'titleDatasR'     => [ 'title' => $title, 'subTitle' => $subTitle, 'breadcrumb' => $breadcrumb, 'align' => 'right' ],
				'titleDatasC'     => [ 'title' => $title, 'subTitle' => $subTitle, 'breadcrumb' => $breadcrumb, 'align' => 'center' ],
				'titleDatasRD'    => [ 'title' => $title, 'subTitle' => $subTitle, 'breadcrumb' => $breadcrumb, 'align' => 'right', 'dark' => TRUE ],
				'titleDatasLP'    => [ 'title' => $title, 'subTitle' => $subTitle, 'breadcrumb' => $breadcrumb, 'pattern' => TRUE ],
				'titleDatasPBG'   => [ 'title' => $title, 'subTitle' => $subTitle, 'breadcrumb' => $breadcrumb, 'align' => 'center', 'img' => 'https://www.notre-planete.info/actualites/images/animaux/ecureuil-gris.jpg', 'backgroundSize' => 'cover' ],
				'titleDatasNBG'   => [ 'title' => $title, 'subTitle' => $subTitle, 'breadcrumb' => $breadcrumb, 'noBg' => TRUE ],
				'titleDatasDPBGM' => [ 'title' => $title, 'subTitle' => $subTitle, 'breadcrumb' => $breadcrumb, 'align' => 'center', 'mini' => TRUE, 'dark' => TRUE, 'img' => 'https://www.notre-planete.info/actualites/images/animaux/ecureuil-gris.jpg', 'backgroundSize' => 'cover' ],
				'titleDatasVBG'   => [ 'title' => $title, 'subTitle' => $subTitle, 'breadcrumb' => $breadcrumb, 'dark' => TRUE, 'parallaxVid' => [ 'vidImg' => 'canvas/images/videos/explore.jpg', 'vid' => [ 'canvas/images/videos/explore.mp4', 'video/mp4' ], 'vidAlt' => [ 'canvas/images/videos/explore.webm', 'video/webm' ] ] ],
			] );
		}
		
	}
