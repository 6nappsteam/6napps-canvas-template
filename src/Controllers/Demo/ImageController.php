<?php

	namespace Sixnapps\CanvasTemplateBundle\Controllers\Demo;
	
	use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
	
	class ImageController extends AbstractController
	{
		public function images()
		{
			return $this->render( '@SixnappsCanvasTemplate/components/image/images.html.twig' );
		}
		
	}
