<?php
	
	namespace Sixnapps\CanvasTemplateBundle\Controllers\Demo;
	
	use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
	
	/**
	 * Class DatePickerController
	 *
	 * @package Sixnapps\CanvasTemplateBundle\Controllers
	 */
	class DatePickerController extends AbstractController
	{
		/**
		 * @return \Symfony\Component\HttpFoundation\Response
		 */
		public function datePicker()
		{
			$datePickers = [
				[
					'label'     => 'Par défaut',
					'parameter' => 'default',
				],
				[
					'label'     => 'Avec la date du jour en focus',
					'parameter' => 'today',
				],
				[
					'label'     => 'Avec une icone',
					'parameter' => 'past-enabled',
					'icon'      => TRUE,
				],
				[
					'label'     => 'Range',
					'parameter' => 'today',
					'range'     => TRUE,
				],
				[
					'label'     => 'Date Format',
					'parameter' => 'format',
				],
				[
					'label'     => 'Fermeture auto désactivé',
					'parameter' => 'autoclose',
				],
				[
					'label'     => 'Les dimanches sont désactivés',
					'parameter' => 'disabled-week',
				],
				[
					'label'     => 'Met en focus les dimanches',
					'parameter' => 'highlighted-week',
				],
				[
					'label'     => 'Choisir plusieurs dates',
					'parameter' => 'multidate',
				],
				[
					'label'     => 'Uniquement les mois',
					'parameter' => 'mnth',
				],
				[
					'label'  => 'Calendrier',
					'inline' => TRUE,
				],
				[
					'label'     => 'Calendrier avec un paramètre',
					'parameter' => 'today',
					'inline'    => TRUE,
				],
			];
			
			return $this->render( '@SixnappsCanvasTemplate/date-picker.html.twig', [
				'datePickers' => $datePickers,
			] );
		}
	}
