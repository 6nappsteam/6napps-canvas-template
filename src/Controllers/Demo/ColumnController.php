<?php
	
	namespace Sixnapps\CanvasTemplateBundle\Controllers\Demo;
	
	use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
	
	/**
	 * Class ColumnController
	 *
	 * @package Sixnapps\CanvasTemplateBundle\Controllers
	 */
	class ColumnController extends AbstractController
	{
		/**
		 * @return \Symfony\Component\HttpFoundation\Response
		 */
		public function column()
		{
			
			$columns = [
				[
					'parameter' => 'col_full',
					'title'     => 'Full width',
				],
				[
					'parameter' => 'col_half',
					'title'     => 'Half width',
				],
				[
					'parameter' => 'col_half col_last',
					'title'     => 'Half width ( last )',
				],
				[
					'parameter' => 'col_two_third',
					'title'     => '2/3 Width',
				],
				[
					'parameter' => 'col_one_third col_last',
					'title'     => '1/3 Width last',
				],
				[
					'parameter' => 'col_one_fourth',
					'title'     => '1/4 Width',
				],
				[
					'parameter' => 'col_three_fourth col_last',
					'title'     => '3/4 Width ( last )',
				],
				[
					'parameter' => 'col_two_fifth',
					'title'     => '2/5 Width',
				],
				[
					'parameter' => 'col_three_fifth col_last',
					'title'     => '3/5 Width ( last )',
				],
				[
					'parameter' => 'col_one_fifth',
					'title'     => '1/5 Width',
				],
				[
					'parameter' => 'col_one_fifth',
					'title'     => '1/5 Width',
				],
				[
					'parameter' => 'col_one_fifth',
					'title'     => '1/5 Width',
				],
				[
					'parameter' => 'col_one_fifth',
					'title'     => '1/5 Width',
				],
				[
					'parameter' => 'col_one_fifth col_last',
					'title'     => '1/5 Width ( last )',
				],
				[
					'parameter' => 'col_one_sixth',
					'title'     => '1/6 Width',
				],
				[
					'parameter' => 'col_five_sixth col_last',
					'title'     => '5/6 Width (Last)',
				],
			];
			
			return $this->render( '@SixnappsCanvasTemplate/column.html.twig', [
				'columns'      => $columns,
				'showGridBoot' => TRUE,
			] );
		}
	}
