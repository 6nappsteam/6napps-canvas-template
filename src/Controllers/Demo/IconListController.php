<?php
	
	namespace Sixnapps\CanvasTemplateBundle\Controllers\Demo;
	
	use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
	
	/**
	 * Class IconListController
	 *
	 * @package Sixnapps\CanvasTemplateBundle\Controllers
	 */
	class IconListController extends AbstractController
	{
		/**
		 * @return \Symfony\Component\HttpFoundation\Response
		 */
		public function iconList()
		{
			
			$icons = [
				[
					'parameter' => 'icon-download2',
				],
				[
					'parameter' => 'icon-inbox',
				],
				[
					'parameter' => 'icon-repeat',
				],
				[
					'parameter' => 'icon-lock3',
				],
				[
					'parameter' => 'icon-tint',
				],
				[
					'parameter' => 'icon-play',
				],
				[
					'parameter' => 'icon-ok-sign',
				],
				[
					'parameter' => 'icon-screenshot',
				],
				[
					'parameter' => 'icon-asterisk',
				],
				[
					'parameter' => 'icon-gift',
				],
				[
					'parameter' => 'icon-leaf',
				],
				[
					'parameter' => 'icon-warning-sign',
				],
				[
					'parameter' => 'icon-calendar3',
				],
				[
					'parameter' => 'icon-plane',
				],
				[
					'parameter' => 'icon-magnet',
				],
				[
					'parameter' => 'icon-folder-close',
				],
				[
					'parameter' => 'icon-bar-chart',
				],
				[
					'parameter' => 'icon-truck2',
				],
				[
					'parameter' => 'icon-cogs',
				],
				[
					'parameter' => 'icon-comments',
				],
				[
					'parameter' => 'icon-trophy',
				],
				[
					'parameter' => 'icon-phone-sign',
				],
				[
					'parameter' => 'icon-credit',
				],
				[
					'parameter' => 'icon-bell',
				],
				[
					'parameter' => 'icon-bullhorn2',
				],
				[
					'parameter' => 'icon-globe',
				],
				[
					'parameter' => 'icon-wrench',
				],
				[
					'parameter' => 'icon-tasks',
				],
				[
					'parameter' => 'icon-filter',
				],
				[
					'parameter' => 'icon-briefcase',
				],
				[
					'parameter' => 'icon-legal',
				],
				[
					'parameter' => 'icon-dashboard',
				],
				[
					'parameter' => 'icon-undo',
				],
				[
					'parameter' => 'icon-filter',
				],
				[
					'parameter' => 'icon-sitemap',
				],
				[
					'parameter' => 'icon-paste',
				],
				[
					'parameter' => 'icon-lightbulb',
				],
				[
					'parameter' => 'icon-cloud-upload',
				],
				[
					'parameter' => 'icon-coffee2',
				],
				[
					'parameter' => 'icon-food2',
				],
				[
					'parameter' => 'icon-beer',
				],
				[
					'parameter' => 'icon-plus-sign2',
				],
				[
					'parameter' => 'icon-desktop',
				],
				[
					'parameter' => 'icon-laptop2',
				],
				[
					'parameter' => 'icon-tablet2',
				],
				[
					'parameter' => 'icon-quote-left',
				],
				[
					'parameter' => 'icon-reply',
				],
				[
					'parameter' => 'icon-smile',
				],
				[
					'parameter' => 'icon-keyboard',
				],
				[
					'parameter' => 'icon-question',
				],
				[
					'parameter' => 'icon-info',
				],
				[
					'parameter' => 'icon-microphone2',
				],
				[
					'parameter' => 'icon-shield',
				],
				[
					'parameter' => 'icon-rocket',
				],
				[
					'parameter' => 'icon-unlock-alt',
				],
			
			];
			
			return $this->render( '@SixnappsCanvasTemplate/icon-list.html.twig', [
				'icons' => $icons,
			] );
		}
	}
