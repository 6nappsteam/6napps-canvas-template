<?php
	
	namespace Sixnapps\CanvasTemplateBundle\Controllers\Demo;
	
	use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
	
	/**
	 * Class SecurityController
	 *
	 * @package Sixnapps\CanvasTemplateBundle\Controllers\Demo
	 */
	class SecurityController extends AbstractController
	{
		/**
		 * @return \Symfony\Component\HttpFoundation\Response
		 */
		public function login()
		{
			return $this->render( '@SixnappsCanvasTemplate/login.html.twig' );
		}
		
		
		/**
		 * @return \Symfony\Component\HttpFoundation\Response
		 */
		public function register()
		{
			return $this->render( '@SixnappsCanvasTemplate/register.html.twig' );
		}
	}
