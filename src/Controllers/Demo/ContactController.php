<?php
	
	namespace Sixnapps\CanvasTemplateBundle\Controllers\Demo;
	
	use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
	
	/**
	 * Class ContactController
	 *
	 * @package Sixnapps\CanvasTemplateBundle\Controllers
	 */
	class ContactController extends AbstractController
	{
		/**
		 * @return \Symfony\Component\HttpFoundation\Response
		 */
		public function contact1()
		{
			return $this->render( '@SixnappsCanvasTemplate/contact-1.html.twig' );
		}
		
		
		/**
		 * @return \Symfony\Component\HttpFoundation\Response
		 */
		public function contact2()
		{
			return $this->render( '@SixnappsCanvasTemplate/contact-2.html.twig' );
		}
		
		
		/**
		 * @return \Symfony\Component\HttpFoundation\Response
		 */
		public function contact3()
		{
			return $this->render( '@SixnappsCanvasTemplate/contact-3.html.twig' );
		}
		
		
		/**
		 * @return \Symfony\Component\HttpFoundation\Response
		 */
		public function contact4()
		{
			return $this->render( '@SixnappsCanvasTemplate/contact-4.html.twig' );
		}
		
		
		/**
		 * @return \Symfony\Component\HttpFoundation\Response
		 */
		public function contact5()
		{
			return $this->render( '@SixnappsCanvasTemplate/contact-5.html.twig' );
		}
		
		
		/**
		 * @return \Symfony\Component\HttpFoundation\Response
		 */
		public function contact6()
		{
			return $this->render( '@SixnappsCanvasTemplate/contact-6.html.twig' );
		}
		
		
		/**
		 * @return \Symfony\Component\HttpFoundation\Response
		 */
		public function contact7()
		{
			return $this->render( '@SixnappsCanvasTemplate/contact-7.html.twig' );
		}
		
		
		/**
		 * @return \Symfony\Component\HttpFoundation\Response
		 */
		public function contactCustom1()
		{
			return $this->render( '@SixnappsCanvasTemplate/contact-custom-1.html.twig' );
		}
		
	}
