<?php
	
	namespace Sixnapps\CanvasTemplateBundle\Controllers\Demo;
	
	use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
	
	/**
	 * Class ErrorController
	 *
	 * @package Sixnapps\CanvasTemplateBundle\Controllers
	 */
	class ErrorController extends AbstractController
	{
		/**
		 * @return \Symfony\Component\HttpFoundation\Response
		 */
		public function error()
		{
			$error = [
//				'type' => 'simple',
//				'type' => 'parallax',
'type'     => 'video',
'title'    => 'Oups veuillez nous excuser, nous n\'avons pas pu trouver la page',
'subtitle' => 'Essayer en utilisant la barre de recherche ci-dessous',
'image'    => 'static.jpg',
'video'    => 'explore',
			];
			
			return $this->render( '@SixnappsCanvasTemplate/404.html.twig', [
				'error' => $error,
			] );
		}
	}
