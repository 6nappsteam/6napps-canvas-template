<?php
	
	namespace Sixnapps\CanvasTemplateBundle\Controllers\Demo;
	
	use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
	
	/**
	 * Class ParallaxController
	 *
	 * @package Sixnapps\CanvasTemplateBundle\Controllers
	 */
	class ParallaxController extends AbstractController
	{
		/**
		 * @return \Symfony\Component\HttpFoundation\Response
		 */
		public function parallax()
		{
			$parallax = [
				[
					'image' => 'portfolio/parallax/1.jpg',
					'title' => 'Open Imagination',
					'tags'  => [
						[
							'name' => 'Danger',
							'link' => '#',
						],
						[
							'name' => 'Risk',
							'link' => '#',
						],
					],
				],
				[
					'image' => 'portfolio/parallax/2.jpg',
					'title' => 'Locked Steel Gate',
					'tags'  => [
						[
							'name' => 'Locked',
							'link' => '#',
						],
						[
							'name' => 'Steel',
							'link' => '#',
						],
					],
				],
				[
					'image' => 'portfolio/parallax/3.jpg',
					'title' => 'Mac Sunglasses',
					'tags'  => [
						[
							'name' => 'Sun',
							'link' => '#',
						],
						[
							'name' => 'Glasses',
							'link' => '#',
						],
					],
				],
				[
					'image' => 'portfolio/parallax/4.jpg',
					'title' => 'Morning Dew',
					'tags'  => [
						[
							'name' => 'morning',
							'link' => '#',
						],
						[
							'name' => 'dew',
							'link' => '#',
						],
					],
				],
				[
					'image' => 'portfolio/parallax/9.jpg',
					'title' => 'Bridge Side',
					'tags'  => [
						[
							'name' => 'bridge',
							'link' => '#',
						],
						[
							'name' => 'game',
							'link' => '#',
						],
					],
				],
				[
					'image' => 'portfolio/parallax/6.jpg',
					'title' => 'Shake It!',
					'tags'  => [
						[
							'name' => 'shake',
							'link' => '#',
						],
						[
							'name' => 'shaker',
							'link' => '#',
						],
					],
				],
				[
					'image' => 'portfolio/parallax/7.jpg',
					'title' => 'Backpack Contents',
					'tags'  => [
						[
							'name' => 'back',
							'link' => '#',
						],
						[
							'name' => 'content',
							'link' => '#',
						],
					],
				],
				[
					'image' => 'portfolio/parallax/10.jpg',
					'title' => 'Study Table',
					'tags'  => [
						[
							'name' => 'table',
							'link' => '#',
						],
						[
							'name' => 'chair',
							'link' => '#',
						],
					],
				],
			];
			
			return $this->render( '@SixnappsCanvasTemplate/parallax.html.twig', [
				'parallax' => $parallax,
			] );
		}
	}
