<?php
	
	namespace Sixnapps\CanvasTemplateBundle\Controllers\Demo;
	
	use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
	
	/**
	 * Class FooterController
	 *
	 * @package Sixnapps\CanvasTemplateBundle\Controllers
	 */
	class FooterController extends AbstractController
	{
		
		/**
		 * @return \Symfony\Component\HttpFoundation\Response
		 */
		public function stickyFooter()
		{
			return $this->render( '@SixnappsCanvasTemplate/sticky-footer.html.twig' );
		}
		
		
		/**
		 * @return \Symfony\Component\HttpFoundation\Response
		 */
		public function footer2()
		{
			$owner = [
				'nom'          => 'Canvas 6nApps',
				'email'        => 'contact@6napps.com',
				'phone'        => '01 02 03 04 05',
				'address'      => [
					'name'    => 'Canvas 6nApps',
					'street'  => '4 rue du docteur Roux',
					'zipcode' => '79000',
					'city'    => 'Niort',
					'country' => 'France',
				],
				'social_media' => [
					'facebook' => 'https://www.facebook.com/',
					'twitter'  => 'https://twitter.com/',
					'youtube'  => 'https://www.youtube.com/',
				],
			];
			$blocs = [
				[
					'type'         => 'contact',
					'footer_title' => 'Nous Contacter',
					'address'      => [
						'name'    => 'Canvas 6nApps',
						'street'  => '4 rue du docteur Roux',
						'city'    => 'Niort',
						'zipCode' => '79000',
						'country' => 'France',
					],
					'contacts'     => [
						[
							'title' => 'telephone',
							'body'  => '01 02 03 04 05',
						],
						[
							'title' => 'email',
							'body'  => 'contact@6napps.com',
						],
					],
				],
				[
					'type'         => 'informations',
					'footer_title' => 'Informations',
					'items'        => [
						[
							'libelle' => 'Conditions Générales d\'utlisations',
							'link'    => '#',
						],
						[
							'libelle' => 'Conditions Générales de vente',
							'link'    => '#',
						],
						[
							'libelle' => 'Mentions légales',
							'link'    => '#',
						],
						[
							'libelle' => 'Confidentialité',
							'link'    => '#',
						],
					],
				],
				[
					'type'         => 'newsletter',
					'footer_title' => 'Newsletters',
					'items'        => [
						[
							'libelle' => 'Newsletter canvas',
						],
						[
							'libelle' => 'Newsletter 6nApps',
						],
					],
				
				],
				[
					'type'         => 'reseaux-sociaux',
					'footer_title' => 'Réseaux sociaux',
					'socials'      => [
						[
							'label' => 'facebook',
							'link'  => 'http://facebook.com',
						],
						[
							'label' => 'twitter',
							'link'  => 'http://twitter.com',
						],
						[
							'label' => 'google',
							'link'  => 'http://google.com',
						],
						[
							'label' => 'reddit',
							'link'  => 'http://reddit.com',
						],
						[
							'label' => 'instagram',
							'link'  => 'http://instagram.com',
						],
					],
					'parameter'    => [
						'rounded',
						'dark',
					],
				],
			];
			
			return $this->render( '@SixnappsCanvasTemplate/footer-2.html.twig', [
				'blocs' => $blocs,
				'owner' => $owner,
			] );
		}
	}
