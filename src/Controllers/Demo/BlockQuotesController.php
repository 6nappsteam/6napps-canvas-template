<?php
	
	namespace Sixnapps\CanvasTemplateBundle\Controllers\Demo;
	
	use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
	
	/**
	 * Class BlockQuotesController
	 *
	 * @package Sixnapps\CanvasTemplateBundle\Controllers
	 */
	class BlockQuotesController extends AbstractController
	{
		/**
		 * @return \Symfony\Component\HttpFoundation\Response
		 */
		public function blockQuotes()
		{
			$blocks = [
				[
					'float'   => 'left',
					'path'    => 'images/team/5.jpg',
					'content' => '« La crise que traverse l’Église aujourd’hui est due, dans une large mesure, à la répercussion, dans l’Église elle-même et dans la vie de ses membres, d’un ensemble de mutations sociales et culturelles rapides, profondes et qui ont une dimension mondiale. Nous sommes en train de changer de monde et de société. Un monde s’efface et un autre est en train d’émerger, sans qu’existe aucun modèle préétabli pour sa construction. Des équilibres anciens sont en train de disparaître, et les équilibres nouveaux ont du mal à se constituer. Or, par toute son histoire, spécialement en Europe, l’Église se trouve assez profondément solidaire des équilibres anciens et de la figure du monde qui s’efface. Non seulement elle y était bien insérée, mais elle avait largement contribué à sa constitution, tandis que la figure du monde qu’il s’agit de construire nous échappe. »',
					'author'  => 'Proposer la foi dans la société actuelle, Lettre aux catholiques de France, 1996, p. 23.',
				],
			];
			
			return $this->render( '@SixnappsCanvasTemplate/block-quotes.html.twig', [
				'blocks' => $blocks,
			] );
		}
	}
