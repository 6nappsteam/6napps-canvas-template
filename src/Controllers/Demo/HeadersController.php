<?php
	
	namespace Sixnapps\CanvasTemplateBundle\Controllers\Demo;
	
	use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
	
	class HeadersController extends AbstractController
	{
		public function onePage()
		{
			return $this->render( '@SixnappsCanvasTemplate/headers.html.twig' );
		}
		
		
		/**
		 * @return \Symfony\Component\HttpFoundation\Response
		 */
		public function headers()
		{
			return $this->render( '@SixnappsCanvasTemplate/headers.html.twig' );
		}
		
	}
