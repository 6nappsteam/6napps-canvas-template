<?php
	
	namespace Sixnapps\CanvasTemplateBundle\Controllers\Demo;
	
	use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
	
	/**
	 * Class AboutUsController
	 *
	 * @package Sixnapps\CanvasTemplateBundle\Controllers
	 */
	class AboutUsController extends AbstractController
	{
		/**
		 * @return \Symfony\Component\HttpFoundation\Response
		 */
		public function about1()
		{
			return $this->render( '@SixnappsCanvasTemplate/about-1.html.twig' );
		}
		
		
		/**
		 * @return \Symfony\Component\HttpFoundation\Response
		 */
		public function about2()
		{
			return $this->render( '@SixnappsCanvasTemplate/about-2.html.twig' );
		}
		
		
		/**
		 * @return \Symfony\Component\HttpFoundation\Response
		 */
		public function aboutMe()
		{
			return $this->render( '@SixnappsCanvasTemplate/about-me.html.twig' );
		}
	}
