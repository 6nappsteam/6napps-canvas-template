<?php
	
	namespace Sixnapps\CanvasTemplateBundle\Controllers\Demo;
	
	use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
	
	class RatingController extends AbstractController
	{
		public function ratings()
		{
			return $this->render( '@SixnappsCanvasTemplate/ratings.html.twig' );
		}
	}
