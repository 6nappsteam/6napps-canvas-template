<?php
	
	namespace Sixnapps\CanvasTemplateBundle\Controllers\Demo;
	
	use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
	
	/**
	 * Class ModalOnloadController
	 *
	 * @package Sixnapps\CanvasTemplateBundle\Controllers
	 */
	class ModalOnloadController extends AbstractController
	{
		/**
		 * @return \Symfony\Component\HttpFoundation\Response
		 */
		public function simple()
		{
			$modal = [
				
				'type'    => 'simple',
				'title'   => 'Un simple titre',
				'content' => 'Un petit contenu pas piqué des hannetons qui fera plaisir à lire, si tenté que vous soyez friands de ce genre de contenu !',
			
			];
			
			return $this->render( '@SixnappsCanvasTemplate/modal-onload.html.twig', [
				'modal' => $modal,
			] );
		}
		
		
		/**
		 * @return \Symfony\Component\HttpFoundation\Response
		 */
		public function iframe()
		{
			$modal = [
				'type'   => 'iframe',
				'title'  => 'Nos vacances',
				'url'    => 'https://player.vimeo.com/video/139208268?badge=0',
				'source' => 'Video en provenance de vimeo',
			];
			
			return $this->render( '@SixnappsCanvasTemplate/modal-onload.html.twig', [
				'modal' => $modal,
			] );
		}
		
		
		/**
		 * @return \Symfony\Component\HttpFoundation\Response
		 */
		public function subscribe()
		{
			$modal = [
				'type'     => 'subscribe',
				'title'    => 'Newsletter souscription',
				'subtitle' => 'Recevez les dernières mises à jour et offres sur la mode',
			];
			
			return $this->render( '@SixnappsCanvasTemplate/modal-onload.html.twig', [
				'modal' => $modal,
			] );
		}
		
		
		/**
		 * @return \Symfony\Component\HttpFoundation\Response
		 */
		public function commonHeight()
		{
			$modal = [
				'type' => 'common-height',
			];
			
			return $this->render( '@SixnappsCanvasTemplate/modal-onload.html.twig', [
				'modal' => $modal,
			] );
		}
		
		
		/**
		 * @return \Symfony\Component\HttpFoundation\Response
		 */
		public function cookies()
		{
			$modal = [
				'type'    => 'cookies',
				'title'   => 'Exemple simple de modal avec autorisation de cookies',
				'content' => 'En poursuivant votre navigation sur ce site, vous acceptez l’utilisation de Cookies pour vous proposer [Par exemple, des publicités ciblées adaptés à vos centres d’intérêts] et réaliser des statistiques de visites.',
			];
			
			return $this->render( '@SixnappsCanvasTemplate/modal-onload.html.twig', [
				'modal' => $modal,
			] );
		}
	}
