<?php
	
	namespace Sixnapps\CanvasTemplateBundle\Controllers\Demo;
	
	use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
	
	class MediasEmbedController extends AbstractController
	{
		public function mediasEmbed()
		{
			
			$medias = [
				[
					'title'  => 'Vimeo Embed',
					'path'   => 'http://player.vimeo.com/video/94842405',
					'width'  => 500,
					'height' => 281,
					'type'   => 'vimeo',
				
				],
				[
					'title'  => 'Youtube Embed',
					'path'   => 'http://www.youtube.com/embed/SZEflIVnhH8',
					'width'  => 560,
					'height' => 315,
					'type'   => 'youtube',
				
				],
				[
					'title'  => 'Dailymotion Embed',
					'path'   => 'http://www.dailymotion.com/embed/video/x1abbei',
					'width'  => 560,
					'height' => 315,
					'type'   => 'dailymotion',
				
				],
				[
					'title'  => 'Soundcloud Embed',
					'path'   => 'https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/115823769&amp;auto_play=false&amp;hide_related=true&amp;visual=true',
					'width'  => '100%',
					'height' => NULL,
					'type'   => 'soundcloud',
				
				],
			
			];
			
			return $this->render( '@SixnappsCanvasTemplate/medias-embed.html.twig', [
				'medias' => $medias,
			] );
		}
	}
