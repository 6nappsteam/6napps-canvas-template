<?php
	
	namespace Sixnapps\CanvasTemplateBundle\Controllers\Demo;
	
	use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
	
	class TogglesAccordionsController extends AbstractController
	{
		public function togglesAccordions()
		{
			
			$content = [
				[
					'open'    => TRUE,
					'title'   => 'This is a Title',
					'content' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Assumenda, dolorum, vero ipsum molestiae minima odio quo voluptate illum excepturi quam cum voluptates doloribus quae nisi tempore necessitatibus dolores ducimus enim libero eaque explicabo suscipit animi at quaerat aliquid ex expedita perspiciatis? Saepe, aperiam, nam unde quas beatae vero vitae nulla.',
				
				],
				[
					'open'    => FALSE,
					'title'   => 'This is a Title',
					'content' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Assumenda, dolorum, vero ipsum molestiae minima odio quo voluptate illum excepturi quam cum voluptates doloribus quae nisi tempore necessitatibus dolores ducimus enim libero eaque explicabo suscipit animi at quaerat aliquid ex expedita perspiciatis? Saepe, aperiam, nam unde quas beatae vero vitae nulla.',
				
				],
				[
					'open'    => FALSE,
					'title'   => 'This is a Title',
					'content' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Assumenda, dolorum, vero ipsum molestiae minima odio quo voluptate illum excepturi quam cum voluptates doloribus quae nisi tempore necessitatibus dolores ducimus enim libero eaque explicabo suscipit animi at quaerat aliquid ex expedita perspiciatis? Saepe, aperiam, nam unde quas beatae vero vitae nulla.',
				
				],
			];
			
			$toggles = [
				[
					'type'    => 'default', // default, background, border
					'toggles' => $content,
				],
				[
					'type'    => 'background', // default, background, border
					'toggles' => $content,
				],
				[
					'type'    => 'border', // default, background, border
					'toggles' => $content,
				],
			];
			
			$accordions = [
				[
					'type'    => 'default', // default, background, border, bootstrap (component accordion-bootstrap)
					'content' => $content,
				],
				[
					'type'    => 'background', // default, background, border, bootstrap (component accordion-bootstrap)
					'content' => $content,
				],
				[
					'type'    => 'border', // default, background, border, bootstrap (component accordion-bootstrap)
					'content' => $content,
				],
				[
					'type'    => 'bootstrap', // default, background, border, bootstrap (component accordion-bootstrap)
					'content' => $content,
				],
			];
			
			$collapses = [
				[
					'type'    => 'href',
					'label'   => 'Link with href',
					'content' => $content[ 0 ][ 'content' ],
				],
				[
					'type'    => 'data',
					'label'   => 'Button with data-target',
					'content' => $content[ 0 ][ 'content' ],
				],
			];
			
			return $this->render( '@SixnappsCanvasTemplate/toggles-accordions.html.twig', [
				'toggles'    => $toggles,
				'accordions' => $accordions,
				'collapses'  => $collapses,
			] );
		}
	}
