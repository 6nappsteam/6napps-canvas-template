<?php
	
	namespace Sixnapps\CanvasTemplateBundle\Controllers\Demo;
	
	use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
	
	/**
	 * Class ThumbnailsSliderController
	 *
	 * @package Sixnapps\CanvasTemplateBundle\Controllers
	 */
	class ThumbnailsSliderController extends AbstractController
	{
		/**
		 * @return \Symfony\Component\HttpFoundation\Response
		 */
		public function thumbnailsSlider()
		{
			$thumbnails = [
				[
					'type'   => 'thumbnails',
					'images' => [
						'portfolio/full/1.jpg',
						'portfolio/full/7.jpg',
						'portfolio/full/10.jpg',
						'portfolio/full/8.jpg',
					],
				],
				[
					'type'   => 'slider',
					'images' => [
						'portfolio/single/full/1.jpg',
						'portfolio/single/full/7.jpg',
						'portfolio/single/full/10.jpg',
					],
				],
				[
					'type'   => 'fading-thumbs',
					'images' => [
						[
							'full'  => 'portfolio/single/full/1.jpg',
							'thumb' => 'portfolio/single/slider-thumbs/1.jpg',
						],
						[
							'full'  => 'portfolio/single/full/7.jpg',
							'thumb' => 'portfolio/single/slider-thumbs/7.jpg',
						],
						[
							'full'  => 'portfolio/single/full/10.jpg',
							'thumb' => 'portfolio/single/slider-thumbs/10.jpg',
						],
					],
				],
				[
					'type'   => 'thumbs-grid',
					'images' => [
						[
							'full'  => 'wedding/11.jpg',
							'thumb' => 'wedding/thumb/11.jpg',
						],
						[
							'full'  => 'wedding/15.jpg',
							'thumb' => 'wedding/thumb/15.jpg',
						],
						[
							'full'  => 'wedding/21.jpg',
							'thumb' => 'wedding/thumb/21.jpg',
						],
						[
							'full'  => 'wedding/23.jpg',
							'thumb' => 'wedding/thumb/23.jpg',
						],
						[
							'full'  => 'wedding/26.jpg',
							'thumb' => 'wedding/thumb/26.jpg',
						],
						[
							'full'  => 'wedding/27.jpg',
							'thumb' => 'wedding/thumb/27.jpg',
						],
					],
				],
				[
					'type'      => 'slider-with-content',
					'images'    => [
						'portfolio/4/1.jpg',
						'portfolio/4/7.jpg',
						'portfolio/4/10.jpg',
					],
					'content'   => '<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Amet, repellendus, quibusdam minima fugiat unde ex cumque velit quod culpa itaque architecto dolorem quis corporis iste recusandae. Dolorum, at, aspernatur, ut, molestias eum natus deserunt id tempora et unde atque dolorem modi sed laudantium eius reiciendis ratione perspiciatis fugit iure illo.</p><p class="nobottommargin">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quam, quaerat, sequi, architecto consequuntur quae placeat expedita facilis reprehenderit facere accusamus asperiores nam odit neque excepturi aut a dolor quisquam reiciendis dignissimos at molestiae tenetur fugit fuga. Vitae, numquam, assumenda cupiditate sint quam asperiores quae aut nemo ipsa ut mollitia rem necessitatibus officiis eveniet cum deleniti dolorum nostrum odio laudantium inventore autem dolore amet enim magni vel? Vero voluptas maxime dolore.</p>',
					'parameter' => 'alignleft',
					//					'parameter' => 'alignright'
				],
			];
			
			return $this->render( '@SixnappsCanvasTemplate/thumbnails-slider.html.twig', [
				'thumbnails' => $thumbnails,
			] );
		}
	}
