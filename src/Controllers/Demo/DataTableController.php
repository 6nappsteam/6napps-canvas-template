<?php
	
	namespace Sixnapps\CanvasTemplateBundle\Controllers\Demo;
	
	use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
	
	/**
	 * Class DataTableController
	 *
	 * @package Sixnapps\CanvasTemplateBundle\Controllers
	 */
	class DataTableController extends AbstractController
	{
		/**
		 * @return \Symfony\Component\HttpFoundation\Response
		 */
		public function dataTable()
		{
			$dataTable = [
				[
					'head'       => [
						'Name',
						'Position',
						'Office',
						'Age',
						'Start date',
						'Salary',
					],
					'foot'       => [
						'Name',
						'Position',
						'Office',
						'Age',
						'Start date',
						'Salary',
					],
					'body'       => [
						[
							'values' => [
								'John Doe',
								'Developer',
								'London',
								'27',
								'26/02/2018',
								'1,000,000€',
							],
						],
						[
							'values' => [
								'John Doe',
								'Developer',
								'London',
								'27',
								'26/02/2018',
								'1,000,000€',
							],
						],
						[
							'values' => [
								'John Doe',
								'Developer',
								'London',
								'27',
								'26/02/2018',
								'1,000,000€',
							],
						],
						[
							'values' => [
								'John Doe',
								'Developer',
								'London',
								'27',
								'26/02/2018',
								'1,000,000€',
							],
						],
						[
							'values' => [
								'John Doe',
								'Developer',
								'London',
								'27',
								'26/02/2018',
								'1,000,000€',
							],
						],
						[
							'values' => [
								'John Doe',
								'Developer',
								'London',
								'27',
								'26/02/2018',
								'1,000,000€',
							],
						],
						[
							'values' => [
								'John Doe',
								'Developer',
								'London',
								'27',
								'26/02/2018',
								'1,000,000€',
							],
						],
						[
							'values' => [
								'John Doe',
								'Developer',
								'London',
								'27',
								'26/02/2018',
								'1,000,000€',
							],
						],
						[
							'values' => [
								'John Doe',
								'Developer',
								'London',
								'27',
								'26/02/2018',
								'1,000,000€',
							],
						],
						[
							'values' => [
								'John Doe',
								'Developer',
								'London',
								'27',
								'26/02/2018',
								'1,000,000€',
							],
						],
						[
							'values' => [
								'John Doe',
								'Developer',
								'London',
								'27',
								'26/02/2018',
								'1,000,000€',
							],
						],
						[
							'values' => [
								'John Doe',
								'Developer',
								'London',
								'27',
								'26/02/2018',
								'1,000,000€',
							],
						],
						[
							'values' => [
								'John Doe',
								'Developer',
								'London',
								'27',
								'26/02/2018',
								'1,000,000€',
							],
						],
						[
							'values' => [
								'John Doe',
								'Developer',
								'London',
								'27',
								'26/02/2018',
								'1,000,000€',
							],
						],
						[
							'values' => [
								'John Doe',
								'Developer',
								'London',
								'27',
								'26/02/2018',
								'1,000,000€',
							],
						],
						[
							'values' => [
								'John Doe',
								'Developer',
								'London',
								'27',
								'26/02/2018',
								'1,000,000€',
							],
						],
						[
							'values' => [
								'John Doe',
								'Developer',
								'London',
								'27',
								'26/02/2018',
								'1,000,000€',
							],
						],
						[
							'values' => [
								'John Doe',
								'Developer',
								'London',
								'27',
								'26/02/2018',
								'1,000,000€',
							],
						],
						[
							'values' => [
								'John Doe',
								'Developer',
								'London',
								'27',
								'26/02/2018',
								'1,000,000€',
							],
						],
						[
							'values' => [
								'John Doe',
								'Developer',
								'London',
								'27',
								'26/02/2018',
								'1,000,000€',
							],
						],
						[
							'values' => [
								'John Doe',
								'Developer',
								'London',
								'27',
								'26/02/2018',
								'1,000,000€',
							],
						],
						[
							'values' => [
								'John Doe',
								'Developer',
								'London',
								'27',
								'26/02/2018',
								'1,000,000€',
							],
						],
						[
							'values' => [
								'John Doe',
								'Developer',
								'London',
								'27',
								'26/02/2018',
								'1,000,000€',
							],
						],
						[
							'values' => [
								'John Doe',
								'Developer',
								'London',
								'27',
								'26/02/2018',
								'1,000,000€',
							],
						],
						[
							'values' => [
								'John Doe',
								'Developer',
								'London',
								'27',
								'26/02/2018',
								'1,000,000€',
							],
						],
						[
							'values' => [
								'John Doe',
								'Developer',
								'London',
								'27',
								'26/02/2018',
								'1,000,000€',
							],
						],
						[
							'values' => [
								'John Doe',
								'Developer',
								'London',
								'27',
								'26/02/2018',
								'1,000,000€',
							],
						],
						[
							'values' => [
								'John Doe',
								'Developer',
								'London',
								'27',
								'26/02/2018',
								'1,000,000€',
							],
						],
						[
							'values' => [
								'John Doe',
								'Developer',
								'London',
								'27',
								'26/02/2018',
								'1,000,000€',
							],
						],
						[
							'values' => [
								'John Doe',
								'Developer',
								'London',
								'27',
								'26/02/2018',
								'1,000,000€',
							],
						],
						[
							'values' => [
								'John Doe',
								'Developer',
								'London',
								'27',
								'26/02/2018',
								'1,000,000€',
							],
						],
						[
							'values' => [
								'John Doe',
								'Developer',
								'London',
								'27',
								'26/02/2018',
								'1,000,000€',
							],
						],
						[
							'values' => [
								'John Doe',
								'Developer',
								'London',
								'27',
								'26/02/2018',
								'1,000,000€',
							],
						],
						[
							'values' => [
								'John Doe',
								'Developer',
								'London',
								'27',
								'26/02/2018',
								'1,000,000€',
							],
						],
						[
							'values' => [
								'John Doe',
								'Developer',
								'London',
								'27',
								'26/02/2018',
								'1,000,000€',
							],
						],
						[
							'values' => [
								'John Doe',
								'Developer',
								'London',
								'27',
								'26/02/2018',
								'1,000,000€',
							],
						],
						[
							'values' => [
								'John Doe',
								'Developer',
								'London',
								'27',
								'26/02/2018',
								'1,000,000€',
							],
						],
						[
							'values' => [
								'John Doe',
								'Developer',
								'London',
								'27',
								'26/02/2018',
								'1,000,000€',
							],
						],
						[
							'values' => [
								'John Doe',
								'Developer',
								'London',
								'27',
								'26/02/2018',
								'1,000,000€',
							],
						],
						[
							'values' => [
								'John Doe',
								'Developer',
								'London',
								'27',
								'26/02/2018',
								'1,000,000€',
							],
						],
						[
							'values' => [
								'John Doe',
								'Developer',
								'London',
								'27',
								'26/02/2018',
								'1,000,000€',
							],
						],
						[
							'values' => [
								'John Doe',
								'Developer',
								'London',
								'27',
								'26/02/2018',
								'1,000,000€',
							],
						],
						[
							'values' => [
								'John Doe',
								'Developer',
								'London',
								'27',
								'26/02/2018',
								'1,000,000€',
							],
						],
						[
							'values' => [
								'John Doe',
								'Developer',
								'London',
								'27',
								'26/02/2018',
								'1,000,000€',
							],
						],
						[
							'values' => [
								'John Doe',
								'Developer',
								'London',
								'27',
								'26/02/2018',
								'1,000,000€',
							],
						],
						[
							'values' => [
								'John Doe',
								'Developer',
								'London',
								'27',
								'26/02/2018',
								'1,000,000€',
							],
						],
						[
							'values' => [
								'John Doe',
								'Developer',
								'London',
								'27',
								'26/02/2018',
								'1,000,000€',
							],
						],
						[
							'values' => [
								'John Doe',
								'Developer',
								'London',
								'27',
								'26/02/2018',
								'1,000,000€',
							],
						],
						[
							'values' => [
								'John Doe',
								'Developer',
								'London',
								'27',
								'26/02/2018',
								'1,000,000€',
							],
						],
						[
							'values' => [
								'John Doe',
								'Developer',
								'London',
								'27',
								'26/02/2018',
								'1,000,000€',
							],
						],
						[
							'values' => [
								'John Doe',
								'Developer',
								'London',
								'27',
								'26/02/2018',
								'1,000,000€',
							],
						],
						[
							'values' => [
								'John Doe',
								'Developer',
								'London',
								'27',
								'26/02/2018',
								'1,000,000€',
							],
						],
						[
							'values' => [
								'John Doe',
								'Developer',
								'London',
								'27',
								'26/02/2018',
								'1,000,000€',
							],
						],
						[
							'values' => [
								'John Doe',
								'Developer',
								'London',
								'27',
								'26/02/2018',
								'1,000,000€',
							],
						],
						[
							'values' => [
								'John Doe',
								'Developer',
								'London',
								'27',
								'26/02/2018',
								'1,000,000€',
							],
						],
						[
							'values' => [
								'John Doe',
								'Developer',
								'London',
								'27',
								'26/02/2018',
								'1,000,000€',
							],
						],
						[
							'values' => [
								'John Doe',
								'Developer',
								'London',
								'27',
								'26/02/2018',
								'1,000,000€',
							],
						],
						[
							'values' => [
								'John Doe',
								'Developer',
								'London',
								'27',
								'26/02/2018',
								'1,000,000€',
							],
						],
					],
					'parameters' => [
						'striped',
						'bordered',
					],
				],
			];
			
			return $this->render( '@SixnappsCanvasTemplate/data-table.html.twig', [
				'dataTable' => $dataTable,
			] );
		}
	}
