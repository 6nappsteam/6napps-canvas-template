<?php
	namespace Sixnapps\CanvasTemplateBundle\Controllers\Demo;
	
	use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
	
	class SectionController extends AbstractController
	{
		public function sections()
		{
			return $this->render( '@SixnappsCanvasTemplate/sections.html.twig' );
		}
	}
