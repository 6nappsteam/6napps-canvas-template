<?php
	
	namespace Sixnapps\CanvasTemplateBundle\Controllers\Demo;
	
	use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
	
	/**
	 * Class TableController
	 *
	 * @package Sixnapps\CanvasTemplateBundle\Controllers
	 */
	class TableController extends AbstractController
	{
		/**
		 * @return \Symfony\Component\HttpFoundation\Response
		 */
		public function tables()
		{
			$tables = [
				[
					'head'       => [
						'Index',
						'Je',
						'Tu',
					],
					'body'       => [
						[
							'values' => [
								'1',
								'ligne',
								'Je',
							],
						],
						[
							'values' => [
								'2',
								'ligne',
								'Tu',
							],
						],
					],
					'parameters' => [
						'striped',
						'bordered',
					],
				],
				[
					'head'       => [
						'Index',
						'Prénom',
						'Nom',
						'UserName',
					],
					'body'       => [
						[
							'values' => [
								'1',
								'Mark',
								'Otto',
								'@mdo',
							],
							'type'   => 'danger',
						],
						[
							'values' => [
								'2',
								'Jacob',
								'Thornton',
								'@fat',
							],
						],
					],
					'parameters' => [
						'hover',
						'bordered',
					],
				
				],
			];
			
			return $this->render( '@SixnappsCanvasTemplate/table.html.twig', [
				'tables' => $tables,
			] );
		}
	}
