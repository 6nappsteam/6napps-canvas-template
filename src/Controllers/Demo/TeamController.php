<?php
	
	namespace Sixnapps\CanvasTemplateBundle\Controllers\Demo;
	
	use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
	
	class TeamController extends AbstractController
	{
		public function team()
		{
			$team = [
				[
					'name'        => 'JOHN DOE',
					'job'         => 'CEO',
					'image'       => '/canvas/images/team/3.jpg',
					'description' => 'Carbon emissions reductions giving, legitimize amplify non-partisan Aga Khan. Policy dialogue assessment expert free-speech cornerstone disruptor freedom. Cesar Chavez empower.',
					'socials'     => [
						[
							'icon'    => 'facebook',
							'link'    => '#',
							'options' => [ 'small', 'light', 'rounded' ],
							'class'   => 'inline-block',
						],
						[
							'icon'    => 'twitter',
							'link'    => '#',
							'options' => [ 'small', 'light', 'rounded' ],
							'class'   => 'inline-block',
						],
						[
							'icon'    => 'linkedin',
							'link'    => '#',
							'options' => [ 'small', 'light', 'rounded' ],
							'class'   => 'inline-block',
						],
					],
				
				],
				[
					'name'        => 'JOSH CLARK',
					'job'         => 'Co-Founder',
					'image'       => '/canvas/images/team/2.jpg',
					'description' => 'Carbon emissions reductions giving, legitimize amplify non-partisan Aga Khan. Policy dialogue assessment expert free-speech cornerstone disruptor freedom. Cesar Chavez empower.',
					'socials'     => [
						[
							'icon'    => 'facebook',
							'link'    => '#',
							'options' => [ 'small', 'light', 'rounded' ],
							'class'   => 'inline-block',
						],
						[
							'icon'    => 'twitter',
							'link'    => '#',
							'options' => [ 'small', 'light', 'rounded' ],
							'class'   => 'inline-block',
						],
						[
							'icon'    => 'linkedin',
							'link'    => '#',
							'options' => [ 'small', 'light', 'rounded' ],
							'class'   => 'inline-block',
						],
					],
				
				],
				[
					'name'        => 'MARY JANE',
					'job'         => 'Sales',
					'image'       => '/canvas/images/team/8.jpg',
					'description' => 'Carbon emissions reductions giving, legitimize amplify non-partisan Aga Khan. Policy dialogue assessment expert free-speech cornerstone disruptor freedom. Cesar Chavez empower.',
					'socials'     => [
						[
							'icon'    => 'facebook',
							'link'    => '#',
							'options' => [ 'small', 'light', 'rounded' ],
							'class'   => 'inline-block',
						],
						[
							'icon'    => 'twitter',
							'link'    => '#',
							'options' => [ 'small', 'light', 'rounded' ],
							'class'   => 'inline-block',
						],
						[
							'icon'    => 'linkedin',
							'link'    => '#',
							'options' => [ 'small', 'light', 'rounded' ],
							'class'   => 'inline-block',
						],
					],
				
				],
				[
					'name'        => 'NIX MAXWELL',
					'job'         => 'Support',
					'image'       => '/canvas/images/team/4.jpg',
					'description' => 'Carbon emissions reductions giving, legitimize amplify non-partisan Aga Khan. Policy dialogue assessment expert free-speech cornerstone disruptor freedom. Cesar Chavez empower.',
					'socials'     => [
						[
							'icon'    => 'facebook',
							'link'    => '#',
							'options' => [ 'small', 'light', 'rounded' ],
							'class'   => 'inline-block',
						],
						[
							'icon'    => 'twitter',
							'link'    => '#',
							'options' => [ 'small', 'light', 'rounded' ],
							'class'   => 'inline-block',
						],
						[
							'icon'    => 'linkedin',
							'link'    => '#',
							'options' => [ 'small', 'light', 'rounded' ],
							'class'   => 'inline-block',
						],
					],
				
				],
			];
			
			return $this->render( '@SixnappsCanvasTemplate/team.html.twig', [
				'team' => $team,
			] );
		}
		
	}
