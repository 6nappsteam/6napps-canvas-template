<?php

namespace Sixnapps\CanvasTemplateBundle\Model;

use Doctrine\ORM\Mapping as ORM;

/**
 * Class Couleur
 *
 * @package Sixnapps\CanvasTemplateBundle\Model
 */
class Couleur
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="slug", type="string", length=55, nullable=false)
     */
    protected $slug;

    /**
     * @var string
     *
     * @ORM\Column(name="value", type="string", length=55, nullable=false)
     */
    protected $value;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return Couleur
     */
    public function setId(int $id): self
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return string
     */
    public function getSlug(): ?string
    {
        return $this->slug;
    }

    /**
     * @param string $slug
     * @return Couleur
     */
    public function setSlug(string $slug): self
    {
        $this->slug = $slug;

        return $this;
    }

    /**
     * @return string
     */
    public function getValue(): ?string
    {
        return $this->value;
    }

    /**
     * @param string $value
     * @return Couleur
     */
    public function setValue(string $value): self
    {
        $this->value = $value;

        return $this;
    }
}
