<?php

namespace Sixnapps\CanvasTemplateBundle\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Filesystem\Exception\IOException;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpKernel\KernelInterface;

/**
 * Class CanvasTemplateInstallerCommand
 *
 * @package Sixnapps\CanvasTemplateBundle\Command
 */
class CanvasTemplateInstallerCommand extends Command
{
    // the name of the command (the part after "bin/console")
    /**
     * @var string
     */
    protected static $defaultName = 'canvas:install';

    /**
     * @var KernelInterface
     */
    protected $kernel;

    /**
     * @var string
     */
    protected $root;


    /**
     * SetUpCommand constructor.
     *
     * @param KernelInterface $kernel
     * @param string|null $name
     */
    public function __construct(KernelInterface $kernel, ?string $name = NULL)
    {
        parent::__construct($name);
        $this->kernel = $kernel;
        $this->root = $this->kernel->getProjectDir();

    }


    /**
     *
     */
    protected function configure()
    {
        $this
            // the short description shown while running "php bin/console list"
            ->setDescription('Installation of Canvas assets.')
            // the full command description shown when running the command with
            // the "--help" option
            ->setHelp('This command allows you to install Canvas Template assets...');
    }


    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     *
     * @return int|void|null
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        // outputs multiple lines to the console (adding "\n" at the end of each line)
        $output->writeln('<info>Set up CanvasBundle</info>');

        // install Entities
        $this->installEntitites($output);

    }

    /**
     * @param OutputInterface $output
     */
    private function installEntitites(OutputInterface $output)
    {
        $prefix = 'Canvas';
        $entities = [
            'Couleur' => 'couleur',
        ];

        $fileSystem = new Filesystem();
        try {
            //Create Entity Folder if doesn't exist
            if (!$fileSystem->exists($this->root . '/src/Entity')) {
                $fileSystem->mkdir($this->root . '/src/Entity', 0700);
            }

            foreach ($entities as $entity => $name) {
                //Create Entity file if doesn't exist
                if (!$fileSystem->exists($this->root . '/src/Entity/' . $prefix . '' . $entity . '.php')) {
                    $fileSystem->touch($this->root . '/src/Entity/' . $prefix . '' . $entity . '.php', 0700);
                }
                //Write in Entity file
                $fileSystem->dumpFile($this->root . '/src/Entity/' . $prefix . '' . $entity . '.php', $this->dataWrite($prefix, $name, $entity));

                //Create config file for EasyAdmin

                //Create folder easy_admin if doesn't exist
                if (!$fileSystem->exists($this->root . '/config/packages/easy_admin')) {
                    $fileSystem->mkdir($this->root . '/config/packages/easy_admin', 0700);
                }

                //Create entity file
                $file = $this->root . '/config/packages/easy_admin/entity.' . strtolower($prefix) . '_' . strtolower($entity) . '.yaml';
                if (!$fileSystem->exists($file)) {
                    $fileSystem->touch($file, 0700);
                }

                //Write in Entity file
                $fileSystem->dumpFile(
                    $file,
                    $this->configFileWrite($prefix, $name)
                );
            }
            $output->writeln('<info>Entities Created</info>');
        } catch (IOException $exception) {
            $output->writeln("an error occurred while creating your directory at " . $exception->getPath());
        }

        $output->writeln('<info>Set up finished</info>');
    }

    /**
     * @param $prefix
     * @param $name
     * @param $entity
     *
     * @return string
     */
    private
    function dataWrite($prefix, $name, $entity)
    {
        $data = "<?php\n" .
            "    namespace App\Entity;\n\n" .
            "    use Doctrine\ORM\Mapping as ORM;\n" .
            "    use Sixnapps\CanvasTemplateBundle\Model\\" . $entity . " as Model;\n\n" .
            "    /**\n" .
            "     * " . ucfirst($prefix) . ucfirst($name) . "\n" .
            "     *\n" .
            "     * @ORM\Entity\n" .
            "     */\n" .
            "     class " . ucfirst($prefix) . ucfirst($entity) . " extends Model\n" .
            "     {\n" .
            "     }\n";
        return $data;
    }

    /**
     * @param $prefix
     * @param $name
     *
     * @return string
     */
    private
    function configFileWrite($prefix, $name)
    {
        $data = "easy_admin:\n" .
            "    entities:\n" .
            "       " . ucfirst($prefix) . ucfirst($name) . ":\n" .
            "           class: 'App\Entity\\" . ucfirst($prefix) . ucfirst($name) . "'\n";
        return $data;
    }
}
