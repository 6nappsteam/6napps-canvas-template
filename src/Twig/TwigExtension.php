<?php

namespace Sixnapps\CanvasTemplateBundle\Twig;

use Sixnapps\CanvasTemplateBundle\Templating\Templating;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class TwigExtension
 *
 * @package Sixnapps\CanvasTemplateBundle\Twig
 */
class TwigExtension extends \Twig_Extension
{

    /**
     * @var Templating
     */
    private $templating;
    /**
     * @var Container
     */
    private $container;

    /**
     * TwigExtension constructor.
     *
     * @param Templating $templating
     */
    public function __construct( Templating $templating, ContainerInterface $container )
    {
        $this->templating = $templating;
        $this->container = $container;
    }


    /**
     * @return array|\Twig_Function[]
     */
    public function getFunctions()
    {
        return [
            new \Twig_SimpleFunction('canvas_component', [ $this, 'canvas_component' ], [ 'is_safe' => [ 'html' ] ]),
            new \Twig_SimpleFunction('canvas_asset', [ $this, 'canvas_asset' ], [ 'is_safe' => [ 'html' ] ]),
            new \Twig_SimpleFunction('adjustBrightness', [ $this, 'adjustBrightness' ], [ 'is_safe' => [ 'html' ] ]),
            new \Twig_SimpleFunction('default', [ $this, 'default' ], [ 'is_safe' => [ 'html' ] ]),

        ];
    }

    public function default( $name )
    {
        return $this->container->getParameter($name);
    }

    /**
     * @param $hex
     * @param $steps
     * @return string
     */
    function adjustBrightness( $hex, $steps )
    {
        // Steps should be between -255 and 255. Negative = darker, positive = lighter
        $steps = max(-255, min(255, $steps));

        // Normalize into a six character long hex string
        $hex = str_replace('#', '', $hex);
        if ( strlen($hex) == 3 ) {
            $hex = str_repeat(substr($hex, 0, 1), 2) . str_repeat(substr($hex, 1, 1), 2) . str_repeat(substr($hex, 2, 1), 2);
        }

        // Split into three parts: R, G and B
        $color_parts = str_split($hex, 2);
        $return = '#';

        foreach ( $color_parts as $color ) {
            $color = hexdec($color); // Convert to decimal
            $color = max(0, min(255, $color + $steps)); // Adjust color
            $return .= str_pad(dechex($color), 2, '0', STR_PAD_LEFT); // Make two char hex code
        }

        return $return;
    }


    /**
     * @param       $item
     * @param array $data
     *
     * @return mixed
     */
    public function canvas_component( $item, $data = [] )
    {
        return $this->templating->render($item, $data);
    }


    /**
     * @param $file
     *
     * @return string
     */
    public function canvas_asset( $file )
    {
        return '/bundles/sixnappscanvastemplate/' . $file;
    }
}
