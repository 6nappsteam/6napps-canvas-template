<?php
	
	namespace Sixnapps\CanvasTemplateBundle\Templating;
	
	use Symfony\Component\Templating\EngineInterface;
	
	/**
	 * Class Templating
	 *
	 * @package Sixnapps\CanvasTemplateBundle\Templating
	 */
	class Templating
	{
		/**
		 * @var EngineInterface
		 */
		private $templating;
		
		/**
		 * @var array
		 */
		private $components;
		
		
		/**
		 * Templating constructor.
		 *
		 * @param EngineInterface $templating
		 */
		public function __construct( EngineInterface $templating )
		{
			$this->templating = $templating;
			
			$this->components = [
				// *** titles ***
				'title'                         => 'title/title',
				
				// *** map ***
				'map'                           => 'map/google-map',
				
				// *** page-menu ***
				'page-menu'                     => 'menu/page-menu',
				
				// *** breadcrumb ***
				'breadcrumb'                    => 'breadcrumb/breadcrumb',
				
				// *** sitemap ***
				'sitemap'                       => 'sitemap/sitemap',
				
				// *** contact ***
				'contact-form'                  => 'contact/contact-form',
				'contact-form-2'                => 'contact/contact-form-2',
				'contact-sidebar'               => 'contact/contact-sidebar',
				'contact-bottom'                => 'contact/contact-bottom',
				'contact-left'                  => 'contact/contact-left',
				'contact-info'                  => 'contact/contact-info',
				
				// *** login-register ***
				'login-form'                    => 'login-register/login',
				'register-form'                 => 'login-register/register',
				'login-page'                    => 'login-register/login-page',
				'login-fosuser'                 => 'login-register/fosuser/security/login',
				'register-fosuser'              => 'login-register/fosuser/registration/register',
				
				// *** list ***
				'link-list-simple'              => 'link-list/simple',
				'item-list'                     => 'item-list/item-list',
				
				// *** carousel ***
				'carousel-simple'               => 'carousel/simple',
				'carousel-advanced'             => 'carousel/advanced',
				
				// *** subscribe ***
				'subscribe-newsletter'          => 'widgets/subscribe/newsletter',
				
				// *** social ***
				'social-icons'                  => 'widgets/social/social-icons',
				
				// *** twitter ***
				'twitter-feed'                  => 'widgets/twitter/twitter-feed',
				
				// *** testimonial ***
				'testimonials'                  => 'widgets/testimonials/testimonials',
				
				// *** promo ***
				'promo'                         => 'widgets/promo/promo',
				
				// *** instagram ***
				'instagram'                     => 'widgets/instagram/instagram',
				
				// *** headers ***
				'header-light'                  => 'header/light',
				'header-light-custom-1'         => 'header/light-custom-1',
				'header'                        => 'header/header',
				'one-page'                      => 'header/one-page',
				'app-landing'                   => 'app-landing',
				
				// *** footers ***
				'sticky-footer'                 => 'footer/sticky-footer',
				'footer-2-custom'               => 'footer/footer-2-custom',
				'footer-2'                      => 'footer/footer-2',
				
				// modal popover tooltip notification
				'modal'                         => 'modal-popovers/modal-item',
				'modal-button'                  => 'modal-popovers/modal-button',
				'modal-button-light'            => 'modal-popovers/modal-button-light',
				'modal-button-border'           => 'modal-popovers/modal-button-border',
				'notification'                  => 'modal-popovers/notification-item',
				'tooltip'                       => 'modal-popovers/tooltip-item',
				'popover'                       => 'modal-popovers/popover-item',
				'modal-simple'                  => 'modal-popovers/modal-simple',
				
				// toggle accordion
				'toggle'                        => 'toggles-accordions/toggle-item',
				'accordion'                     => 'toggles-accordions/accordion-item',
				'accordion-bootstrap'           => 'toggles-accordions/accordion-bootstrap-item',
				'collapse'                      => 'toggles-accordions/collapse-item',
				
				// radio switch
				'radio'                         => 'radios-switches/radio-item',
				'switch'                        => 'radios-switches/switch-item',
				
				// *** Featured boxes ***
				'boxe-item'                     => 'boxes/boxes-item',
				'boxe-carousel'                 => 'boxes/boxes-carousel',
				'boxe-custom-1'                 => 'boxes/boxe-custom-1',
				'boxe-custom-2'                 => 'boxes/boxe-custom-2',
				
				// *** Animation ***
				'animation'                     => 'animation/item',
				'animation-counter-list'        => 'animation/animation-counter-list',
				'animation-skill-bar'           => 'animation/animation-skill-bar',
				
				// *** Tabs ***
				'tabs'                          => 'tabs/tabs-item',
				
				// *** Table ***
				'table'                         => 'table/table-item',
				
				// *** Data Table ***
				'data-table'                    => 'data-table/table',
				
				// *** Date & Time Picker ***
				'date-picker'                   => 'date-picker/date-picker-item',
				'date-picker-icon'              => 'date-picker/date-picker-item-icon',
				'date-picker-inline'            => 'date-picker/date-picker-item-inline',
				'date-picker-range'             => 'date-picker/date-picker-item-range',
				
				// *** Gallery ***
				'gallery'                       => 'gallery/item-columns',
				
				// *** Columns & Grid ***
				'columns'                       => 'column/column',
				'column'                        => 'column/item',
				'gridBoot'                      => 'column/gridBoot',
				
				// *** FAQ ***
				'faq'                           => 'faq/faq-item',
				'media-embed'                   => 'medias-embed/media-item',
				'social-icon'                   => 'social-icons/social-icon-item',
				'share-panel'                   => 'social-icons/share-panel',
				
				// *** TEAM ***
				'team'                          => 'team/team-item',
				'team-big'                      => 'team/team-big',
				'team-list'                     => 'team/team-list',
				
				// *** PORTFOLIO ***
				'portfolio'                     => 'portfolio/portfolio',
				'portfolio-2'                   => 'portfolio/portfolio-2',
				
				// *** GRID ***
				'grid-client'                   => 'grid/grid-client',
				
				// flip-cards
				'flip-card-petite'              => 'flip-cards/flip-card-petite',
				'flip-card-moyenne'             => 'flip-cards/flip-card-moyenne',
				'flip-card-grande'              => 'flip-cards/flip-card-grande',
				'flip-card-custom-1'            => 'flip-cards/flip-card-custom-1',
				
				// *** Icon List ***
				'icon-list'                     => 'icon-list/item',
				
				// *** Style Boxe ***
				'style-boxe-native'             => 'style-boxe/item',
				'style-boxe-extended'           => 'style-boxe/item-extended',
				'style-boxe-bootstrap'          => 'style-boxe/item-bootstrap',
				
				// *** CUSTOM ***
				'custom-link'                   => 'footer/custom-link',
				'custom-newsletter'             => 'footer/custom-newsletter',
				'custom-contact'                => 'footer/custom-contact',
				
				//Video
				'video-custom-1'                => 'video/video-custom-1',
				
				//Image
				'image-component-1'             => 'image/image-component-1',
				'image-component-2'             => 'image/image-component-2',
				'image-component-3'             => 'image/image-component-3',
				
				// *** Maintenance ***
				'maintenance'                   => 'maintenance/maintenance',
				
				// *** Error 404 ***
				'error-simple'                  => 'error/simple-404',
				'error-parallax'                => 'error/parallax-404',
				'error-video'                   => 'error/video-404',
				
				//Section
				'section-custom-1'              => 'section/section-custom-1',
				
				//Rating
				'rating-custom-1'               => 'rating/rating-custom-1',
				
				// *** Block & Quotes ***
				'block-quotes'                  => 'block-quotes/block-quotes',
				
				// *** THUMBNAILS & SLIDER ***
				'thumbnails-slider'             => 'thumbnails-slider/thumbnails-slider',
				'slider'                        => 'slider/slider',
				
				//Comments
				'comment-custom-1'              => 'comment/comment-custom-1',
				
				// *** Modal on load ***
				'modal-onload-simple'           => 'modal-onload/simple',
				'modal-onload-iframe'           => 'modal-onload/iframe',
				'modal-onload-subscribe'        => 'modal-onload/subscribe',
				'modal-onload-common-height'    => 'modal-onload/common-height',
				'modal-onload-cookies'          => 'modal-onload/cookies',
				
				// *** Careers ***
				'careers-item'                  => 'careers/item',
				'careers-form'                  => 'careers/form',
				
				// *** Carousel ***
				'carousel-image'                => 'carousel/carousel-image',
				'carousel-portfolio'            => 'carousel/carousel-portfolio',
				'carousel-event'                => 'carousel/carousel-event',
				'carousel-portfolio-full'       => 'carousel/carousel-portfolio-full',
				
				// *** Parallax ***
				'parallax'                      => 'parallax/item',
				
				// *** Parallax ***
				'corporate1'                    => 'corporate/corporate1',
				'corporate1-custom-row-1'       => 'corporate/custom-row-1',
				'corporate1-custom-row-2'       => 'corporate/custom-row-2',
				'corporate1-custom-section-1'   => 'corporate/custom-section-1',
				'corporate1-custom-portfolio-1' => 'corporate/custom-portfolio-1',
				
				// *** Service ***
				'service-item-image'            => 'service/item-image',
				'service-item-center'           => 'service/item-center',
				
				// *** Documentation ***
				'documentation'                 => 'documentation/documentation',
				
				// *** Sidebar ***
				'sidebar'                       => 'sidebar/sidebar',
				'content-sidebar-1'             => 'sidebar/content_sidebar_1',
				'content-sidebar-2'             => 'sidebar/content_sidebar_2',
			];
		}
		
		
		/**
		 * @param $item
		 * @param $data
		 *
		 * @return mixed
		 */
		public function render( $item, $data )
		{
			if ( isset( $this->components[ $item ] ) && $this->templating->exists( '@SixnappsCanvasTemplate/components/' . $this->components[ $item ] . '.html.twig' ) ) {
				return $this->templating->render( '@SixnappsCanvasTemplate/components/' . $this->components[ $item ] . '.html.twig',
					$data );
			}
			return $this->templating->render(
				'@SixnappsCanvasTemplate/components/not-found.html.twig', [
					'name' => $item,
				]
			);
		}
	}
